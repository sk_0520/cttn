#!/bin/bash

if [ ${EUID:-${UID}} -ne 0 ]; then
    echo "need to be root to perform"
    exit 1
fi

pushd ~

cd /tmp
mkdir cttn-cloned
cd cttn-cloned
wget http://www.sqlite.org/2014/sqlite-autoconf-3080500.tar.gz
tar -xf sqlite-autoconf-3080500.tar.gz
cd sqlite-autoconf-3080500
./configure
make
make install
cd ../

wget https://github.com/downloads/libevent/libevent/libevent-2.0.21-stable.tar.gz
tar -xf libevent-2.0.21-stable.tar.gz
cd libevent-2.0.21-stable
./configure
make
make install
cd ../
ldconfig
yum install -y ImageMagick ImageMagick-devel
yum install -y openssl-devel
ln -s /usr/local/lib/libevent-2.0.so.5 /usr/lib64/libevent-2.0.so.5
ln -s /usr/local/lib/libevent_pthreads-2.0.so.5 /usr/lib64/libevent_pthreads-2.0.so.5

popd

