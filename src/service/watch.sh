#!/bin/bash

if [ ${EUID:-${UID}} -ne 0 ]; then
    echo "need to be root to perform"
    exit 1
fi

INI_CTTN=/etc/cttn/cttn.conf

BIN_CTTN=cttn.cgi
APP_CTTN=/home/slave/cttn-app

echo "CTTN監視開始"
sleep 5

while true
do
    CTTN_ALIVE=`ps -ef | grep -F $BIN_CTTN | grep -v grep | wc -l`
    if [ $CTTN_ALIVE -eq 0 ] ; then
        echo "$BIN_CTTN死亡"
        date
        sudo /etc/init.d/cttn start
    fi
    sleep 30
done


