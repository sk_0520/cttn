#!/bin/bash

if [ ${EUID:-${UID}} -ne 0 ]; then
    echo "need to be root to perform"
    exit 1
fi

chmod 774 cttn
chmod 774 *.sh
chmod 774 sleeper/*.sh

# cron設定
crontab -l > /tmp/cron.dat
echo "@reboot /etc/init.d/cttn start > /dev/null 2>&1 &" >> /tmp/cron.dat
sort /tmp/cron.dat | uniq > /tmp/cron.dat2
crontab /tmp/cron.dat2

# copy
cp cttn /etc/init.d/ -f




