#!/bin/bash
# コンパイルしなくても変更できるデータの入れ替え

CTTN_DIR=/home/slave
CTTN_APP=$CTTN_DIR/cttn-app

pushd /home/worker/cttn/src/cttn
    echo "静的ファイル置き換え"
    sudo rsync -rltvz --delete public $CTTN_APP/
    sudo chown -R slave:worker $CTTN_APP/public
popd


