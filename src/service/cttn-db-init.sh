#!/bin/bash

if [ ${EUID:-${UID}} -ne 0 ]; then
    echo "need to be root to perform"
    exit 1
fi

PATH_DIR=/home/slave/cttn-dat

PATH_DATA=$PATH_DIR/data.sqlite3
#PATH_DATA=/home/worker/cttn/src/~cttn-data/db.sqlite3
PATH_LOG=$PATH_DIR/log.sqlite3

dbinit() {
    local DB=$1

    if [ ! -e $DB ] ; then
        DB_DIR=`dirname $DB`
        mkdir -p $DB_DIR
        chown slave:worker $DB_DIR
        chmod 774 $DB_DIR
        touch $DB
        chown slave:worker $DB
        chmod 775 $DB
    else
        : > $DB
    fi
}



# データDB作成
dbinit $PATH_DATA
sqlite3 $PATH_DATA << EOT

create table BLOG_HEAD
(
    BLOG_ID       integer  primary key,
    BLOG_TITLE    text     not null,
    CMN_CREATE    datetime not null,
    CMN_UPDATE    datetime not null,
    CMN_DELETE    datetime,
    CMN_REVISION  integer  not null
)
;

create table BLOG_BODY
(
    BLOG_ID       integer  primary key,
    BLOG_CONTENT  text     not null
)
;

create table BLOG_TAG
(
    BLOG_ID       integer,
    BLOG_TAG_NAME text,
    primary key(
        BLOG_ID,
        BLOG_TAG_NAME
    )
)
;

create table ARTICLE_GROUP
(
    ARTICLE_GROUP_ID    integer  primary key,
    ARTICLE_GROUP_NAME  text     not null unique,
    ARTICLE_GROUP_SEQ   integer  not null unique,
    CMN_CREATE          datetime not null,
    CMN_UPDATE          datetime not null,
    CMN_DELETE          datetime,
    CMN_REVISION        integer  not null
)
;

create table ARTICLE_HEAD
(
    ARTICLE_ID        integer  primary key,
    ARTICLE_GROUP_ID  integer  not null,
    ARTICLE_TITLE     text     not null,
    ARTICLE_SEQ       text     not null,
    CMN_CREATE        datetime not null,
    CMN_UPDATE        datetime not null,
    CMN_DELETE        datetime,
    CMN_REVISION      integer  not null
)
;

create table ARTICLE_BODY
(
    ARTICLE_ID       integer primary key,
    ARTICLE_CONTENT  text    not null
)
;

create table FILE_INFO
(
    FILE_ID       integer  primary key,
    FILE_KEY      text     not null,
    FILE_TITLE    text     not null,
    FILE_PATH     text     not null unique,
    FILE_NAME     text,
    FILE_SIZE     integer  not null,
    CMN_CREATE    datetime not null,
    CMN_UPDATE    datetime not null,
    CMN_DELETE    datetime,
    CMN_REVISION  integer  not null
)
;

EOT

# ログDB作成
dbinit $PATH_LOG
sqlite3 $PATH_LOG << EOT

create table ACCESS_COUNT
(
    ACCESS_PATH    text primary key,
    LOGGING_TIME   datetime,
    CLIENT_IPADDR  text,
    COUNTER        integer not null
)
;

create table ACCESS_LOG
(
    LOGGING_TIME   datetime,
    CLIENT_IPADDR  text,
    CLIENT_HOST    text,
    CLIENT_UA      text,
    ACCESS_PATH    text,
    ACCESS_REQUEST text,
    ACCESS_REFERER text
)
;

create table ERROR_LOG
(
    LOGGING_TIME   datetime,
    CLIENT_IPADDR  text,
    CLIENT_HOST    text,
    CLIENT_UA      text,
    ERROR_CODE     integer,
    ERROR_MSG      text
)
;

EOT


