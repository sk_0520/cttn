#!/bin/bash
# cttnモジュール cttn.cgi を最新版に置き換える。
# 実行準備として ./init.sh を root にて一度だけ実行しておく。
# * sudo の許可は /home/init.sh で実施済み、のはず。

CTTN_DIR=/home/slave
CTTN_APP=$CTTN_DIR/cttn-app
CTTN_DAT=$CTTN_DIR/cttn-dat
CTTN_BAK=$CTTN_DIR/cttn-bak

CTTN_SHELL=watch.sh

pushd /home/worker/cttn/src/cttn
    echo "リリースビルド"
    time ./release.sh
    
    echo "サービス終了"
    sudo /etc/init.d/cttn stop

    echo "ダミーサーバー起動"
    pushd /home/worker/cttn/src/service/sleeper
        sudo ./sleeper.sh &
    popd

    echo "データバックアップ"
    mkdir -p $CTTN_BAK
    pushd $CTTN_DIR
        sudo chmod 775 -R cttn-dat
        sudo chown slave:worker -R cttn-dat
        tar -czvf $CTTN_BAK/dat_`date +'%Y-%m-%dT%H-%M-%S'`.tar.gz cttn-dat
    popd

    echo "DBアナライズ"
    find $CTTN_DAT -type f -name *.sqlite3 | while read FILE_PATH
    do
        echo "reindex: $FILE_PATH"
        sqlite3 "$FILE_PATH" reindex
        echo "analyze: $FILE_PATH"
        sqlite3 "$FILE_PATH" analyze
    done

    echo "cttn置き換え"
    rm -rf  $CTTN_APP
    mkdir -m 775 -p $CTTN_APP
    sudo chown slave:worker $CTTN_APP
    cp cttn.cgi    $CTTN_APP/
    cp -rp public/ $CTTN_APP/
    # 各種シェルの移動
    pushd /home/worker/cttn/src/service
        cp $CTTN_SHELL $CTTN_APP/
    popd
    sudo chown -R slave:worker $CTTN_APP/

    # リリース時になんかしなきゃいけない時のモジュール
    pushd /home/worker/cttn/src/service
        if [ -f sub.sh ] ; then
            echo "sub deploy start"
            chmod 755 sub.sh
            ./sub.sh
        else
            echo "sub deploy none"
        fi
    popd

    # ダミーサーバー終了
    killtree() {
        local _pid=$1
        local _sig=${2:--TERM}
        echo "pid -> ${_pid}"
        for _child in $(ps -o pid --no-headers --ppid ${_pid}); do
            killtree ${_child} ${_sig}
        done
        sudo kill -9 -${_sig} ${_pid}
    }
    echo "KILL sleeper"
    killtree `pgrep sleeper`
    sleep 1

    echo "cttn起動"
    sudo /etc/init.d/cttn start
popd


