"use strict";
/**
マークダウン表示用フック
*/

$(function() {
	var middle = ['.article-content', '.blog-content']
	$('article').find(middle.join(',')).find('.content img').each(attachImage);
})

/**
画像にリンク設定。
*/
function attachImage(index)
{
	var $image  = $(this);
	var $parent = $image.parent();
	var $link   = $('<a>');
	$link.attr('href', $image.attr('src').replace("thumbnail=true", "thumbnail=false"));
	$link.append($image);
	$parent.append($link);
}

