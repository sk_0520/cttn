"use strict";
/**
初っ端。
*/
$(function() {
	setjQueryUI();
	setUniform();
	setTabIndent();
	setAutosize();
});

function setjQueryUI()
{
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
}

function setUniform()
{
	$('input, textarea, select').uniform();
}

function setTabIndent()
{
	tabIndent.renderAll();
}

function setAutosize()
{
	$('textarea.autosize').autosize();
}
