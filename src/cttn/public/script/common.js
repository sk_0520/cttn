"use strict";
/**
共通。
*/


/**
http://stackoverflow.com/questions/1960473/unique-values-in-an-array?answertab=votes#tab-top
*/
function getUnique(array){
	var u = {}, a = [];
		for(var i = 0, l = array.length; i < l; ++i){
			if(u.hasOwnProperty(array[i])) {
				continue;
			}
			a.push(array[i]);
			u[array[i]] = 1;
		}
	return a;
}


