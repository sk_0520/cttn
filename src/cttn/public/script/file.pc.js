"use strict";

function initUploader()
{
	$('#remove').click(toRemove);
	$('#adjust').click(toAdjust);
	$('#file').submit(onSubmit);
	$('#append').click(function() {
		var $tr = $('<tr>');
		var valueList = [
			{tag: 'span',  type: '',     text: '実行', className: ['event', 'remove'], func: toRemove},
			{tag: 'input', type: 'text', text: '',     className: ['title'],           func: null},
			{tag: 'input', type: 'text', text: '',     className: ['name'],            func: null},
			{tag: 'input', type: 'file', text: '',     className: ['upload'],          func: null },
			{tag: 'span',  type: 'file', text: '実行', className: ['event', 'adjust'], func: toAdjust},
			
		];
		for(var i = 0; i < valueList.length; i++) {
			var value = valueList[i];
			var $td = $('<td>');
			var $elm = $('<' + value.tag + '>');
			if(value.type.length) {
				$elm.attr('type', value.type);
			}
			if(value.text.length) {
				$elm.text(value.text);
			}
			for(var j = 0; j < value.className.length; j++) {
				$elm.addClass(value.className[j]);
			}
			if(value.func) {
				$elm.click(value.func);
			}
			$td.append($elm);
			$tr.find('input').uniform();
			$tr.append($td);
		}
		$('#appender').append($tr);
	});
}

function initFileList()
{
	$('td.path .event').click(function() {
		alert('未実装');
	});
	
	$('input[name=update]').datepicker({
		onSelect: function() {
			fileListUpdate($(this).closest('table'));
		}
	}).keypress(function() {
		fileListUpdate($(this).closest('table'));
	});
	
	$('input[name=word]').keydown(function(e) {
		fileListUpdate($(this).closest('table'));
	}).change(function() {
		fileListUpdate($(this).closest('table'));
	});
	
}



function fileListUpdate($table)
{
	var $parent = $table.children('tbody');
	$parent.find('tr').show();
	var findDate = $table.children('thead').find('input[name=update]').val();
	var findWord = $table.children('thead').find('input[name=word]').val();

	//var patternDate = new RegExp(findDate);
	var patternWord = new RegExp(findWord);
	
	function fileListShow($tr, show)
	{
		if(show) {
			$tr.show();
			$tr.next().show();
		} else {
			$tr.hide();
			$tr.next().hide();
		}
	}
	
	$parent.find('tr:even').each(function() {
		var $tr = $(this);
		var params = $tr.find('.params');
		
		var targetDate = params.children('input[name=update]').val();

		var targetTitle = $tr.find('input.title').val();
		var targetName  = $tr.find('input.name').val();

		var show = false;
		if(findDate.length && findWord.length) {
			show
				=  findDate === targetDate
				&& (targetTitle.match(patternWord) != null || targetName.match(patternWord) != null)
			;
		} else if(findDate.length) {
			show = findDate === targetDate;
		} else if(findWord.length) {
			show = (targetTitle.match(patternWord) != null || targetName.match(patternWord) != null);
		}
		
		fileListShow($tr, show);
	});
}

function toRemove()
{
	var $parent = $(this).closest('tr');
	$parent.remove();
}

function getName(path)
{
	return path.replace(/\\/g,'/').replace( /.*\//, '' );
}

function toAdjust()
{
	var $parent = $(this).closest('tr');
	var $upload = $parent.find('input.upload');
	var $title  = $parent.find('input.title');
	var $name   = $parent.find('input.name');
	var filePath = $upload.val();
	var fileName = getName(filePath);
	$title.val(fileName);
	$name.val(fileName);
}

function onSubmit()
{
	// 各行からファイルの存在する部分を列挙してチェック
	var error = false;
	var $file = $('#appender');
	$file.find('tr').each(function(index) {
		var $parent = $(this);
		var $upload = $parent.find('input.upload');
		if(!$upload.val().length) {
			if(index) {
				$parent.remove();
			} else {
				error = true;
			}
			return true;
		}
		var $title  = $parent.find('input.title');
		var $name   = $parent.find('input.name');
		if(!$title.val().length) {
			$title.val(getName($upload.val()));
		}
		if(!$name.val().length) {
			$name.val(getName($upload.val()));
		}
	});
	if(error) {
		return false;
	}

	var dupFileList = $('input.name').map(function() {
		return $(this).val();
	});
	var uniqList = getUnique(dupFileList)
	if(dupFileList.length != uniqList.length) {
		// ファイル名に重複あり
		return false;
	}

	
	var count = 0;
	$file.find('tr').each(function(index) {
		var num = String(index + 1);
		var $parent = $(this);
		$parent.find('input.upload').attr('name', 'upload_file_' + num);
		$parent.find('input.title').attr('name', 'upload_title_' + num);
		$parent.find('input.name').attr('name', 'upload_name_' + num);
		count += 1;
	});
	$('#upload_length').val(count);
	
	return true;
}


