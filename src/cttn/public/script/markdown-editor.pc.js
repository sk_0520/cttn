"use strict";
/**
マークダウンエディタ拡張

https://github.com/jmcmanus/pagedown-extra
*/

$(function() {
	var middle = ['.article-content', '.blog-content']
	$('article').find(middle.join(',')).find('.content img').each(attachImage);
})

function createMarkdownEditor(groupName, options, exFunc, isAdmin)
{
	var converter = Markdown.getSanitizingConverter();
	
	if(exFunc) {
		exFunc(converter);
	}

	converter.hooks.chain('preConversion', function(text) {
		return text.replace(/~~(.*?)~~/g, '<del>$1</del>');
	});
	
	var leadingPipe = new RegExp(
		[
			'^'                         ,
			'[ ]{0,3}'                  , // Allowed whitespace
			'[|]'                       , // Initial pipe
			'(.+)\\n'                   , // $1: Header Row

			'[ ]{0,3}'                  , // Allowed whitespace
			'[|]([ ]*[-:]+[-| :]*)\\n'  , // $2: Separator

			'('                         , // $3: Table Body
			'(?:[ ]*[|].*\\n?)*'      , // Table rows
			')',
			'(?:\\n|$)'                   // Stop at final newline
		].join(''),
		'gm'
	);

	var noLeadingPipe = new RegExp(
		[
			'^'                         ,
			'[ ]{0,3}'                  , // Allowed whitespace
			'(\\S.*[|].*)\\n'           , // $1: Header Row

			'[ ]{0,3}'                  , // Allowed whitespace
			'([-:]+[ ]*[|][-| :]*)\\n'  , // $2: Separator

			'('                         , // $3: Table Body
			'(?:.*[|].*\\n?)*'        , // Table rows
			')'                         ,
			'(?:\\n|$)'                   // Stop at final newline
		].join(''),
		'gm'
	);

	var noSeparatorPipe = new RegExp(
		[
			'^'                         ,
			'[ ]{0,3}'                  , // Allowed whitespace
			'(\\S.*[|].*)\\n'           , // $1: Header Row

			'('                         , // $3: Table Body
			'(?:.*[|].*\\n?)*'        , // Table rows
			')'                         ,
			'(?:\\n|$)'                   // Stop at final newline
		].join(''),
		'gm'
	);

	converter.hooks.chain('postBlockGamut', function(text, runBlockGamut) {
		return text.replace(/[\r\n][\r\n]^ {0,3}""" *[\r\n][\r\n]+((?:.*?[\r\n])+?)[\r\n] {0,3}""" *[\r\n][\r\n]/gm, function (whole, inner) {
			return "<blockquote>" + runBlockGamut(inner) + "</blockquote>\n";
		});
	});
	converter.hooks.chain('postBlockGamut', function(text, runBlockGamut) {
		function doTable(match, header, separator, body, offset, string)
		{
			// remove any leading pipes and whitespace
			header = header.replace(/^ *[|]/m, '');
			if(separator) {
				separator = separator.replace(/^ *[|]/m, '');
			} else {
				body = header + "\n" + body;
			}
			body = body.replace(/^ *[|]/gm, '');

			// remove trailing pipes and whitespace
			header = header.replace(/[|] *$/m, '');
			if(separator) {
				separator = separator.replace(/[|] *$/m, '');
			}
			body = body.replace(/[|] *$/gm, '');

			// determine column alignments
			var align = [];
			if(separator) {
				var alignspecs = separator.split(/ *[|] */);
				for (var i = 0; i < alignspecs.length; i++) {
					var spec = alignspecs[i];
					if (spec.match(/^ *-+: *$/m))
						align[i] = ' class="right"';
					else if (spec.match(/^ *:-+: *$/m))
						align[i] = ' class="center"';
					else if (spec.match(/^ *:-+ *$/m))
						align[i] = ' class="left"';
					else align[i] = '';
				}
			}

			// TODO: parse spans in header and rows before splitting, so that pipes
			// inside of tags are not interpreted as separators
			if(header) {
				var headers = header.split(/ *[|] */);
				var colCount = headers.length;
				if(!separator) {
					for(var i = 0; i < colCount; i++) {
						align[i] = '';
					}
				}
			}

			// build html
			var cls = self.tableClass ? ' class="' + self.tableClass + '"' : '';
			var html = ['<table', cls, '>\n', '<thead>\n', '<tr>\n'].join('');

			// build column headers.
			if(separator) {
				for (i = 0; i < colCount; i++) {
					//var headerHtml = convertSpans(trim(headers[i]), self);
					var headerHtml = headers[i];
					html += ["  <th", align[i], ">", headerHtml, "</th>\n"].join('');
				}
				html += "</tr>\n</thead>\n";
			}

			// build rows
			var rows = body.split('\n');
			for (i = 0; i < rows.length; i++) {
				if (rows[i].match(/^\s*$/)) // can apply to final row
				continue;

				// ensure number of rowCells matches colCount
				var rowCells = rows[i].split(/ *[|] */);
				var lenDiff = colCount - rowCells.length;
				for (var j = 0; j < lenDiff; j++)
				rowCells.push('');

				html += "<tr>\n";
				for (j = 0; j < colCount; j++) {
					//var colHtml = convertSpans(trim(rowCells[j]), self);
					var colHtml = rowCells[j];
					html += ["  <td", align[j], ">", colHtml, "</td>\n"].join('');
				}
				html += "</tr>\n";
			}

			html += "</table>\n";
			//console.log(html);
			// replace html with placeholder until postConversion step
			return html;
		}

		var html = text.replace(leadingPipe, doTable).replace(noLeadingPipe, doTable).replace(noSeparatorPipe, function(match, header, body, offset, string) {
			if(body) {
				return doTable(match, header, null, body, offset, string);
			} else {
				return match;
			}
		});
		return html
	});
	
	var editor = new Markdown.Editor(converter, '-' + groupName, options);
	editor.run();
	var cheatsheetLoad = false;
	$('#cheatsheet-show-' + groupName).click(function() {
		if(!cheatsheetLoad) {
			$('#cheatsheet-viewer-' + groupName).load('/document/markdown.html #choice', null, function() {
				if(!isAdmin) {
					$(this).find('.admin').remove();
				}
				cheatsheetLoad = true;
			});
		} else {
			$('#cheatsheet-viewer-' + groupName).toggle();
		}
	});
	
	$('#preview-post-' + groupName).click(function() {
		var $parent = $('#preview-viewer-' + groupName);
		
		var $eventParent = $('#preview-event-' + groupName);
		if(!$eventParent.find('.remove').length) {
			var $eventItem   = $('<li>');
			var $eventRemove = $('<span>').text('プレビュー画面を閉じる').addClass('event').addClass('remove').click(function() {
				$eventItem.remove();
				$parent.empty();
			});
			$eventItem.append($eventRemove);
			$eventParent.append($eventItem);
		}
		
		$parent.empty();
		var $form   = $('<form>');
		$form.attr('target', 'dummy');
		$form.attr('method', 'post');
		$form.attr('action', '/app/preview');
		$form.append($('<input>').attr('name', 'contentName').attr('type', 'hidden').val(groupName));
		$form.append($('<input>').attr('name', 'contentBody').attr('type', 'hidden').val($('#wmd-input-' + groupName).val()));
		$form.append($('<input>').attr('name', 'inDownload').attr('type', 'hidden').val('#{admin}'));
		var $preview = $('<iframe>').attr('name', 'dummy');
		$form.append($preview);
		$form.append($preview);
		$parent.append($form);
		
		var $mask = $('<div>').addClass('mask');
		$mask.css('left', $preview.offset().left);
		$mask.css('top', $preview.offset().top);
		$mask.width($preview.outerWidth());
		$mask.height($preview.outerHeight());
		$mask.append($('<p>').text('プレビュー画面構築中'));
		$parent.append($mask);
		
		$preview.load(function() {
			var $html = $preview.contents();
			var $contents = $html.find('section.contents');
			if($contents.length) {
				$html.find('h1').remove();
				$html.find('#header').remove();
				$html.find('#navi').remove();
				$html.find('#footer').remove();
				$contents.css('width', '100%');
				$contents.css('padding-top', '0');
				$mask.fadeOut();
			}
		});
		$form.submit();
	});


}



