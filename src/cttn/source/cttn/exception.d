/**
例外関係。
*/
module cttn.exception;

template CttnExceptionBuilder()
{
	this(string msg, string _file=__FILE__, int _line=__LINE__, Throwable next=null)
	{
		super(msg, _file, _line, next);
	}
}

/***/
class CttnException: Exception
{
	mixin CttnExceptionBuilder;
}





class LockException: CttnException
{
	mixin CttnExceptionBuilder;
}



