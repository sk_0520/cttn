/**
拡張ストレージ、
*/
module cttn.app.storage.ex_storage;

import std.variant;

import vibe.d;

import cttn.mvc;
import cttn.app.dto.dto;


/**
*/
enum GetNumber
{
	value,
	count,
	min,
	max,
}


/**
*/
class ExDBStorage: DBStorage
{
	mixin DBStorageBuilder;

	TSize getNumber(TSize = sizediff_t)(string tableName, string columnName, GetNumber num, string whereCommand = string.init, Variant[string] params = null)
	{
		auto expr = this.db.createExprMap();
		expr.setExpr("VALUE", num == GetNumber.value,  columnName);
		expr.setExpr("COUNT", num == GetNumber.count,  "count(" ~ columnName ~ ")");
		expr.setExpr("MIN",   num == GetNumber.min,    "min(" ~ columnName ~ ")");
		expr.setExpr("MAX",   num == GetNumber.max,    "max(" ~ columnName ~ ")");
		expr.setExpr("WHERE", whereCommand.length > 0, "where " ~ whereCommand);
		expr.setExpr("TABLE_NAME",  tableName);
		
		
		auto query = this.db.createQuery!("ExDBStorage.getNumber.sql")(expr);
		//query.setParam("tableName",  tableName);
		query.setParams(params);

		auto rows = query.getResultList();
		auto row = rows.front;
		
		return row["NUM"].as!TSize;
	}
	protected TDto getDtoSingle(TDto: Dto)(DBQuery query) const
	{
		auto dto = new TDto();
		auto row = query.getResultList().front;
		dto.importRow(row);
		return dto;
	}
	
	protected TDto[] getDtoList(TDto: Dto)(DBQuery query) const
	{
		auto app = appender!(TDto[])();
		auto data = query.getResultList();
		foreach(row; data) {
			auto dto = new TDto();
			dto.importRow(row);
			app.put(dto);
		}
		
		return app.data();
	}
}



