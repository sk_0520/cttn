/**
記事。
*/
module cttn.app.storage.article_storage;

import std.string;
import std.variant;

import vibe.d;

import cttn.utility;
import cttn.type;
import cttn.mvc;
public import cttn.app.storage.ex_storage;
import cttn.app.entity.article_entity;
import cttn.app.model.article_base_model;
import cttn.app.dto.article_dto;


/**
*/
class ArticleStorage: ExDBStorage
{
	mixin DBStorageBuilder;

	bool hasGroup(ArticleGroupId groupId)
	{
		auto whereCommand = "ARTICLE_GROUP_ID = :groupId";
		Variant[string] params;
		params["groupId"] = groupId.toOriginalValue();
		auto count = getNumber("ARTICLE_GROUP", "ARTICLE_GROUP_ID", GetNumber.count, whereCommand, params);

		return count > 0;
	}


	bool hasArticle(ArticleId id)
	{
		auto whereCommand = "ARTICLE_ID = :id";
		Variant[string] params;
		params["id"] = id.toOriginalValue();
		auto count = getNumber("ARTICLE_HEAD", "ARTICLE_ID", GetNumber.count, whereCommand, params);

		return count > 0;
	}

	/**
	*/
	bool hasGroupName(ArticleGroupId groupId, string groupName)
	{
		auto whereCommand = "ARTICLE_GROUP_ID != :groupId and ARTICLE_GROUP_NAME = :groupName";
		Variant[string] params;
		params["groupId"] = groupId.toOriginalValue();
		params["groupName"] = groupName;
		auto count = getNumber("ARTICLE_GROUP", "ARTICLE_GROUP_NAME", GetNumber.count, whereCommand, params);

		return count > 0;
	}
	
	/**
	*/
	bool hasPageTitle(ArticleGroupId groupId, ArticleId id, string title)
	{
		auto whereCommand = "ARTICLE_GROUP_ID = :groupId and ARTICLE_ID != :id and ARTICLE_TITLE = :title";
		Variant[string] params;
		params["groupId"] = groupId.toOriginalValue();
		params["id"] = id.toOriginalValue();
		params["title"] = title;
		auto count = getNumber("ARTICLE_HEAD", "ARTICLE_TITLE", GetNumber.count, whereCommand, params);

		return count > 0;
	}
	
	
	/**
	*/
	ArticleGroupDto getGroup(ArticleGroupId groupId, bool scanDelete)
	{
		auto expr = this.db.createExprMap();
		expr.setExpr("DELETE", scanDelete, "", "and CMN_DELETE is null");
		auto query = this.db.createQuery!("ArticleStorage.getGroup.sql")(expr);
		query.setParam("groupId", groupId);
		return getDtoSingle!(ArticleGroupDto)(query);
	}
	/**
	*/
	ArticleGroupDto[] getGroupList(bool scanDelete)
	{
		auto expr = this.db.createExprMap();
		expr.setExpr("DELETE", scanDelete, "", "and CMN_DELETE is null");
		auto query = this.db.createQuery!("ArticleStorage.getGroupList.sql")(expr);
		return getDtoList!(ArticleGroupDto)(query);
	}

	ArticleTitleDto[] getTitleList(ArticleGroupId groupId, bool scanDelete)
	{
		auto expr = this.db.createExprMap();
		expr.setExpr("DELETE", scanDelete, "", "and CMN_DELETE is null");
		auto query = this.db.createQuery!("ArticleStorage.getTitleList.sql")(expr);
		query.setParam("groupId", groupId);
		return getDtoList!(ArticleTitleDto)(query);
	}

	size_t getMaxGroupId()
	{
		return getNumber("ARTICLE_GROUP", "ARTICLE_GROUP_ID", GetNumber.max);
	}
	size_t getMaxGroupSeq()
	{
		return getNumber("ARTICLE_GROUP", "ARTICLE_GROUP_SEQ", GetNumber.max);
	}
	size_t getTargetGroupSeq(ArticleGroupId groupId)
	{
		auto whereCommand = "ARTICLE_GROUP_ID = :groupId";
		Variant[string] params;
		params["groupId"] = groupId.toOriginalValue();
		
		return getNumber("ARTICLE_GROUP", "ARTICLE_GROUP_SEQ", GetNumber.value, whereCommand, params);
	}
	size_t getMaxPageId()
	{
		return getNumber("ARTICLE_HEAD", "ARTICLE_ID", GetNumber.max);
	}
	size_t getMaxPageSeq(ArticleGroupId id)
	{
		auto whereCommand = "ARTICLE_GROUP_ID = :id";
		Variant[string] params;
		params["id"] = id.toOriginalValue();

		auto result = getNumber("ARTICLE_HEAD", "ARTICLE_SEQ", GetNumber.max, whereCommand, params);
		return result;
	}
	size_t getTargetPageSeq(ArticleId id)
	{
		auto whereCommand = "ARTICLE_ID = :id";
		Variant[string] params;
		params["id"] = id.toOriginalValue();
		
		return getNumber("ARTICLE_HEAD", "ARTICLE_SEQ", GetNumber.max, whereCommand, params);
	}
	
	ArticlePageDto getPage(ArticleId id, bool scanDelete)
	{
		auto expr = this.db.createExprMap();
		expr.setExpr("DELETE", scanDelete, "", "and CMN_DELETE is null");
		auto query = this.db.createQuery!("ArticleStorage.getPage.sql")(expr);
		query.setParam("id", id);
		return getDtoSingle!(ArticlePageDto)(query);
	}

	version(none)
	ArtcleMoveList getMoveList(ArticleId id, bool scanDelete)
	{
		auto expr = this.db.createExprMap();
		expr.setExpr("DELETE", scanDelete, "", "and CMN_DELETE is null");
		auto query = this.db.createQuery!("ArticleStorage.getMoveList.sql")(expr);
		query.setParam("id", id);
		return getDtoSingle!(ArtcleMoveList)(query);
	}

	void registGroup(ArticleGroupId groupId, string groupName)
	in
	{
		assert(groupName.length);
	}
	body
	{
		auto entity = new ArticleGroupEntity();
		entity.groupName = groupName;
		entity.cc.setTimestampNow();
		
		if(hasGroup(groupId)) {
			entity.groupId  = groupId;
			entity.sequence = getTargetGroupSeq(groupId);
			
			entity.executeUpdate(this.db);
		} else {
			auto maxId  = getMaxGroupId();
			auto maxSeq = getMaxGroupSeq();
			auto newId  = cast(ArticleGroupId)(maxId + 1);
			auto newSeq = maxSeq + 1;
			
			entity.groupId  = newId;
			entity.sequence = newSeq;
			
			entity.executeInsert(this.db);
		}
	}

	void registArticleContent(ArticleGroupId groupId, ArticleId id, string title, string content)
	in
	{
		assert(groupId != ArticleGroupId.init);
	}
	body
	{
		auto entHead = new ArticleHeadEntity();
		auto entBody = new ArticleBodyEntity();

		entHead.groupId = groupId;
		entHead.title   = title;
		entHead.cc.setTimestampNow();
		entBody.content = content;

		if(hasArticle(id)) {
			entHead.id = id;
			entHead.sequence = getTargetPageSeq(entHead.id);
			entBody.id = id;
			
			entHead.executeUpdate(this.db);
			entBody.executeUpdate(this.db);
		} else {
			auto pageID = cast(ArticleId)(getMaxPageId() + 1);
			entHead.id       = pageID;
			entHead.sequence = getMaxPageSeq(entHead.groupId) + 1;
			entBody.id       = pageID;
			
			entHead.executeInsert(this.db);
			entBody.executeInsert(this.db);
		}
	}

	void switchPageId(ArticleId[] idList)
	in
	{
		assert(idList.length);
	}
	body
	{
		auto time = getNowTime();
		auto timestamp = time.toDBString();

		auto idStmList = appender!(string[])();
		auto keyList = appender!(string[])();
		foreach(i, targetId; idList) {
			auto s = format("id_%s", i);
			keyList.put(s);
			idStmList.put(format("ARTICLE_ID = :%s", s));
		}
		auto expr = this.db.createExprMap();
		expr.setExpr("TARGET_ID", idStmList.data().join(" or "));

		auto query = this.db.createQuery!("ArticleStorage.switchPageId.sql")(expr);
		query.setParam("timestamp", timestamp);
		assert(idList.length == keyList.data().length);
		foreach(i, key; keyList.data()) {
			query.setParam(key, idList[i]);
		}
		query.execute();
	}

}



