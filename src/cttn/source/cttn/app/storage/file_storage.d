/**
ファイル。
*/
module cttn.app.storage.file_storage;

import std.string;
import std.variant;

//import vibe.d;

import cttn.type;
import cttn.utility;
import cttn.mvc;
public import cttn.app.storage.ex_storage;
import cttn.app.model.file_model;
import cttn.app.dto.file_dto;
import cttn.app.entity.file_entity;


/**
*/
class FileStorage: ExDBStorage
{
	mixin DBStorageBuilder;

	private bool hasId(FileId id)
	{
		auto whereCommand = "FILE_ID = :id";
		Variant[string] params;
		params["id"] = id.toOriginalValue();
		auto count = getNumber("FILE_INFO", "FILE_ID", GetNumber.count, whereCommand, params);

		return count > 0;
	}

	FileKeyDto[] getFileKeys(bool scanDelete)
	{
		auto expr = this.db.createExprMap();
		expr.setExpr("DELETE", scanDelete, "", "and CMN_DELETE is null");
		auto query = this.db.createQuery!("FileStorage.getFileKeys.sql")(expr);
		return getDtoList!(FileKeyDto)(query);
	}
	
	FileInfoDto[] getFileInfo(string key, bool scanDelete)
	{
		auto expr = this.db.createExprMap();
		expr.setExpr("DELETE", scanDelete, "", "and CMN_DELETE is null");
		auto query = this.db.createQuery!("FileStorage.getFileInfo.sql")(expr);
		query.setParam("key", key);
		return getDtoList!(FileInfoDto)(query);
	}

	FileDownloadDto getDownload(FileId id, bool scanDelete)
	{
		auto expr = this.db.createExprMap();
		expr.setExpr("DELETE", scanDelete, "", "and CMN_DELETE is null");
		auto query = this.db.createQuery!("FileStorage.getDownload.sql")(expr);
		query.setParam("id", id);
		auto result = getDtoList!(FileDownloadDto)(query);
		if(result.length) {
			auto dto = result[0];
			dto.enable = true;
			return dto;
		} else {
			return new FileDownloadDto();
		}
	}

	size_t getMaxId()
	{
		return getNumber("FILE_INFO", "FILE_ID", GetNumber.max);
	}
	
	void registFilePath(FileId id, string key, string title, string path, string name, ulong size)
	{
		auto entity = new FileInfoEntity();
		entity.key   = key;
		entity.title = title;
		entity.path  = path;
		entity.name  = name;
		entity.size  = size;
		
		entity.cc.setTimestampNow();

		if(id && hasId(id)) {
			entity.id = id;
			entity.executeUpdate(this.db);
		} else {
			auto maxId = getMaxId();
			auto newId = cast(FileId)(maxId + 1);
			
			entity.id = newId;
			entity.executeInsert(this.db);
		}
	}


}



