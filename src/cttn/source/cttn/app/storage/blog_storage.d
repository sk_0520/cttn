/**
ブログ。
*/
module cttn.app.storage.blog_storage;

import std.algorithm;
import std.array;

import vibe.d;

import d2sqlite3;

import cttn.mvc;
import cttn.utility;
import cttn.type;
import cttn.exception;
public import cttn.app.storage.ex_storage;
import cttn.app.dto.blog_dto;
import cttn.app.model.blog_base_model;
import cttn.app.entity.blog_entity;


/**
*/
class BlogStorage: ExDBStorage
{
	mixin DBStorageBuilder;

	private bool hasId(BlogId id)
	{
		auto query = this.db.createQuery!("BlogStorage.hasId.sql")();
		query.setParam("id", id);
		auto result = query.getResultList();
		auto row = result.front;
		return row["NUM"].as!sizediff_t > 0;
	}

	private BlogSimpleTagDto[] getTagList(ref QueryCache rows) const
	{
		auto app = appender!(BlogSimpleTagDto[])();
		foreach(ref row; rows) {
			auto dto = new BlogSimpleTagDto();
			dto.importRow(row);
			app.put(dto);
		}
		return app.data();
	}

	BlogSimpleTagDto[] getTagList()
	{
		auto query = this.db.createQuery!("BlogStorage.getTagList.sql")();
		auto list = query.getResultList();
		return getTagList(list);
	}
	
	BlogSimpleTagDto[] getTagListFromId(BlogId id)
	{
		auto query = this.db.createQuery!("BlogStorage.getTagListFromId.sql")();
		query.setParam("id", id);
		auto list = query.getResultList();
		return getTagList(list);
	}
	
	BlogTagDto[] getTagListFromIdList(BlogId[] idList)
	in
	{
		assert(idList.length);
	}
	body
	{
		auto nameList = new string[idList.length];
		auto paramList = new string[idList.length];
		foreach(i, id; idList) {
			nameList[i]  = format("id_%s", i);
			paramList[i] = format("BLOG_ID = :%s", nameList[i]);
		}
		auto expr = this.db.createExprMap();
		expr.setExpr("ID_LIST", paramList.join(" or "));
		//ID_LIST
		auto query = this.db.createQuery!("BlogStorage.getTagListFromIdList.sql")(expr);
		foreach(i, name; nameList) {
			query.setParam(name, idList[i]);
		}
		auto app = appender!(BlogTagDto[])();
		foreach(row; query.getResultList()) {
			auto dto = new BlogTagDto();
			dto.importRow(row);
			app.put(dto);
		}

		return app.data();
	}

	BlogTitleDto[] getTitleList(ushort year, ushort month, bool orderByAsc, bool scanDelete)
	in
	{
		assert(0 <= year && year <= 9999);
		assert(0 <= month && month <= 12);
	}
	body
	{
		auto expr = this.db.createExprMap();
		expr.setExpr("ORDER", orderByAsc, "asc", "desc");
		expr.setExpr("DELETE", scanDelete, "", "and CMN_DELETE is null");
		auto query = this.db.createQuery!("BlogStorage.getTitleList.sql")(expr);
		string likeDate;
		if(0 < month) {
			likeDate = format("%04s%02s%%", year, month);
		} else {
			likeDate = format("%04s%%", year);
		}
		assert(likeDate.length);
		query.setParam("likeDate", likeDate);

		auto app = appender!(BlogTitleDto[])();
		foreach(row; query.getResultList()) {
			auto dto = new BlogTitleDto();
			dto.importRow(row);
			app.put(dto);
		}
		
		return app.data();
	}

	BlogContentDto getContent(BlogId id, bool scanDelete)
	{
		auto expr = this.db.createExprMap();
		expr.setExpr("DELETE", scanDelete, "", "and CMN_DELETE is null");
		auto query = this.db.createQuery!("BlogStorage.getContent.sql")(expr);
		query.setParam("id", id);
		foreach(row; query.getResultList()) {
			auto dto = new BlogContentDto();
			dto.importRow(row);
			return dto;
		}
		throw new CttnException(to!string(id));
	}

	private DBQuery getContentQuery(string sqlName)(size_t offset, size_t limit, bool orderByAsc, bool scanDelete)
	{
		auto expr = this.db.createExprMap();
		expr.setExpr("DELETE", scanDelete, "", "and CMN_DELETE is null");
		expr.setExpr("ORDER",  orderByAsc, "asc", "desc");
		auto query = this.db.createQuery!(sqlName)(expr);
		query.setParam("offset", offset);
		query.setParam("limit",  limit);

		return query;
	}
	/+
	private BlogContentDto[] getContentList(DBQuery query) const
	{
		auto app = appender!(BlogContentDto[])();
		foreach(row; query.getResultList()) {
			auto dto = new BlogContentDto();
			dto.importRow(row);
			app.put(dto);
		}
		
		return app.data();
	}
	+/

	BlogContentDto[] getContentNormalList(size_t offset, size_t limit, bool orderByAsc, bool scanDelete)
	{
		auto query = getContentQuery!("BlogStorage.getContentNormalList.sql")(offset, limit, orderByAsc, scanDelete);
		auto result = getDtoList!(BlogContentDto)(query);
		return result;
	}

	BlogContentDto[] getContentNormalListFromTag(string tagName, size_t offset, size_t limit, bool orderByAsc, bool scanDelete)
	in
	{
		assert(tagName.length);
	}
	body
	{
		auto query = getContentQuery!("BlogStorage.getContentNormalListFromTag.sql")(offset, limit, orderByAsc, scanDelete);
		query.setParam("tagName", tagName);
		return getDtoList!(BlogContentDto)(query);
	}
	
	BlogContentDto[] getContentArchive(ushort year, ushort month, bool orderByAsc, bool scanDelete)
	{
		auto expr = this.db.createExprMap();
		expr.setExpr("ORDER", orderByAsc, "asc", "desc");
		expr.setExpr("DELETE", scanDelete, "", "and CMN_DELETE is null");
		auto query = this.db.createQuery!("BlogStorage.getContentArchive.sql")(expr);
		string likeDate;
		if(0 < month) {
			likeDate = format("%04s%02s%%", year, month);
		} else {
			likeDate = format("%04s%%", year);
		}
		assert(likeDate.length);
		query.setParam("likeDate", likeDate);
		
		return getDtoList!(BlogContentDto)(query);
	}

	void deleteTagFromId(BlogId id)
	{
		auto query = this.db.createQuery!("BlogStorage.deleteTagFromId.sql")();
		query.setParam("id", id);
		query.execute();
	}

	void registBlogContent(BlogId id, string title, string content, string[] tagNames)
	in
	{
		assert(id, "id");
		assert(title.length, "title");
		assert(content.length, "content");
	}
	body
	{
		auto entHead = new BlogHeadEntity();
		auto entBody = new BlogBodyEntity();
		
		entHead.id    = id;
		entHead.title = title;
		entHead.cc.setTimestampNow();

		entBody.id      = id;
		entBody.content = content;
		
		if(hasId(id)) {
			entHead.executeUpdate(this.db);
			entBody.executeUpdate(this.db);
			deleteTagFromId(id);
		} else {
			entHead.executeInsert(this.db);
			entBody.executeInsert(this.db);
		}
		
		foreach(tagName; tagNames) {
			auto entTag = new BlogTagEntity();
			entTag.id   = id;
			entTag.name = tagName;
			
			entTag.executeInsert(this.db);
		}
	}

	void switchId(BlogId[] idList)
	in
	{
		assert(idList.length);
	}
	body
	{
		auto time = getNowTime();
		auto timestamp = time.toDBString();

		auto idStmList = appender!(string[])();
		auto keyList = appender!(string[])();
		foreach(i, targetId; idList) {
			auto s = format("id_%s", i);
			keyList.put(s);
			idStmList.put(format("BLOG_ID = :%s", s));
		}
		auto expr = this.db.createExprMap();
		expr.setExpr("TARGET_ID", idStmList.data().join(" or "));

		auto query = this.db.createQuery!("BlogStorage.switchId.sql")(expr);
		query.setParam("timestamp", timestamp);
		assert(idList.length == keyList.data().length);
		foreach(i, key; keyList.data()) {
			query.setParam(key, idList[i]);
		}
		query.execute();
		
	}

	BlogYearMonthDto[] getYearMonthList(bool scanDelete)
	{
		auto expr = this.db.createExprMap();
		expr.setExpr("DELETE", scanDelete, "", "and CMN_DELETE is null");
		auto query = this.db.createQuery!("BlogStorage.getYearMonthList.sql")(expr);
		
		auto app = appender!(BlogYearMonthDto[])();
		foreach(row; query.getResultList()) {
			auto dto = new BlogYearMonthDto();
			dto.importRow(row);
			app.put(dto);
		}
		
		return app.data();
	}


}



