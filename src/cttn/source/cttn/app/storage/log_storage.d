/**
ログ関連。
*/
module cttn.app.storage.log_storage;

import std.string;
import std.variant;

import vibe.d;

//import d2sqlite3;

/+
import cttn.type;
import cttn.exception;
+/
import cttn.utility;
import cttn.mvc;
public import cttn.app.storage.ex_storage;
import cttn.app.entity.log_entity;
import cttn.app.dto.log_dto;


/**
*/
class LogStorage: ExDBStorage
{
	mixin DBStorageBuilder;

	bool hasCounter(string path)
	{
		Variant[string] params;
		params["accessPath"] = path;
		return getNumber("ACCESS_COUNT", "ACCESS_PATH", GetNumber.count, "ACCESS_PATH = :accessPath", params) > 0;
	}

	private struct BaseData
	{
		SysTime loggingTime;
		string  clientIpAddress;
		string  clientHost;
		string  clientUserAgent;
		string  accessPath;
		string  accessRequest;
		string  accessReferer;
	
		this(HTTPServerRequest req)
		{
			this.loggingTime = getNowTime();
			this.clientIpAddress = req.clientAddress.toAddressString();
			this.clientHost = string.init;
			this.clientUserAgent = req.headers.get("User-Agent");
			this.accessPath = req.path;
			this.accessRequest = req.queryString;
			this.accessReferer   = req.headers.get("Referer");
		}
	}

	void registerAccess(HTTPServerRequest req)
	{
		auto countEntity = new AccessCountEntity();
		auto logEntity   = new AccessLogEntity();

		auto baseData = BaseData(req);
		
		countEntity.accessPath      = baseData.accessPath;
		countEntity.loggingTime     = baseData.loggingTime;
		countEntity.clientIpAddress = baseData.clientIpAddress;

		logEntity.loggingTime     = baseData.loggingTime;
		logEntity.clientIpAddress = baseData.clientIpAddress;
		logEntity.clientHost      = baseData.clientHost;
		logEntity.clientUserAgent = baseData.clientUserAgent;
		logEntity.accessPath      = baseData.accessPath;
		logEntity.accessRequest   = baseData.accessRequest;
		logEntity.accessReferer   = baseData.accessReferer;
		
		if(hasCounter(baseData.accessPath)) {
			countEntity.executeUpdate(this.db);
		} else {
			countEntity.executeInsert(this.db);
		}
		logEntity.executeInsert(this.db);
	}

	void registerError(HTTPServerRequest req, HTTPServerErrorInfo err)
	{
		auto entity = new ErrorLogEntity();
		
		auto baseData = BaseData(req);
		
		entity.loggingTime     = baseData.loggingTime;
		entity.clientIpAddress = baseData.clientIpAddress;
		entity.clientHost      = baseData.clientHost;
		entity.clientUserAgent = baseData.clientUserAgent;
		entity.errorCode       = err.code;
		entity.errorMessage    = err.message;
		
		entity.executeInsert(this.db);
	}

	LogCountDto[] getPageCount()
	{
		auto query = this.db.createQuery!("LogStorage.getPageCount.sql")();
		return getDtoList!(LogCountDto)(query);
	}
	LogCountDto[] getErrorCount()
	{
		auto query = this.db.createQuery!("LogStorage.getErrorCount.sql")();
		return getDtoList!(LogCountDto)(query);
	}

	LogCountDto[] getBrowserVersionUA()
	{
		auto query = this.db.createQuery!("LogStorage.getBrowserVersionUA.sql")();
		return getDtoList!(LogCountDto)(query);
	}

	LogCountDto[] getBrowserTotalUA()
	{
		auto query = this.db.createQuery!("LogStorage.getBrowserTotalUA.sql")();
		return getDtoList!(LogCountDto)(query);
	}
	
	LogCountDto[] getOsVersionUA()
	{
		auto query = this.db.createQuery!("LogStorage.getOsVersionUA.sql")();
		return getDtoList!(LogCountDto)(query);
	}
	LogCountDto[] getOsTotalUA()
	{
		auto query = this.db.createQuery!("LogStorage.getOsTotalUA.sql")();
		return getDtoList!(LogCountDto)(query);
	}

	LogCountDto[] getReferer(string hostName)
	{
		auto query = this.db.createQuery!("LogStorage.getReferer.sql")();
		auto url = format("http://%s%%", hostName);
		auto www = format("http://www.%s%%", hostName);
		query.setParam("url", url);
		query.setParam("www", url);
		return getDtoList!(LogCountDto)(query);
	}

}



