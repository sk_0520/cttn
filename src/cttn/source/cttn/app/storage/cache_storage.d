/**
キャッシュ領域。
*/
module cttn.app.storage.cache_storage;

import vibe.d;

import cttn.mvc;
import cttn.type;
import cttn.utility;
import cttn.app.dto.cache_dto;

/**
*/
final class SessionStorage: CttnStorage
{
	private Session session;
	
	this(Session session)
	in
	{
		assert(session);
	}
	body
	{
		super(cast(void*)&session);
		this.session = session;
	}

	string getKey(string key)
	{
		auto sessionKey = this.session.get!(string)(key);
		if(sessionKey.length) {
			return sessionKey;
		}
		
		return string.init;
	}

	void setKey(string key, string value)
	{
		this.session.set(key, value);
	}
}


/**
*/
class LoginSessionStorage: DBStorage
{
	mixin DBStorageBuilder;

	Login findLoginSessionKey(string key)
	{
		Login result;
		
		auto findQuery = this.db.createQuery!("LoginSessionStorage.findLoginSessionKey.sql")();
		findQuery.setParam("key", key);
		foreach(row; findQuery.getResultList()) {
			auto dto = new LoginDto();
			dto.importRow(row);
			result.key  = dto.key;
			result.name = dto.name;
			result.lastAccess = dto.access;
			result.isLogin    = true;
			break;
		}

		return result;
	}

	void deleteLoginSession(string name)
	{
		auto deleteQuery = db.createQuery!("LoginSessionStorage.deleteLoginSession.sql");
		deleteQuery.setParam("name", name);
		deleteQuery.execute();
	}

	void insertLoginSession(const ref Login login)
	{
		auto insertQuery = db.createQuery!("LoginSessionStorage.insertLoginSession.sql");
		insertQuery.setParam("key", login.key);
		insertQuery.setParam("name", login.name);
		insertQuery.setParam("access", login.lastAccess.toDBString());
		insertQuery.execute();
	}
}



