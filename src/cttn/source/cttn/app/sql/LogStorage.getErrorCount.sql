--
select
    AC.ACCESS_PATH || ': ' || EL.ERROR_CODE  as NAME,
    sum(COUNTER) as COUNT
from
    ACCESS_COUNT AC
    inner join ERROR_LOG EL
        using(LOGGING_TIME, CLIENT_IPADDR)
group by
    NAME
order by
    COUNTER desc

