-- ブラウザバージョン別こりゃあかん
select
    case
        -- Internet Explorer
        when CLIENT_UA like '%Trident/7.0%rv:11.%' then
            'Internet Explorer 11.x'
        when CLIENT_UA like '%MSIE 10.%' then
            'Internet Explorer 10.x'
        when CLIENT_UA like '%MSIE 4.%' then
            'Internet Explorer 4.x'
        when CLIENT_UA like '%MSIE 9.%' then
            'Internet Explorer 9.x'
        when CLIENT_UA like '%MSIE 8.%' then
            'Internet Explorer 8.x'
        when CLIENT_UA like '%MSIE 7.%' then
            'Internet Explorer 7.x'
        when CLIENT_UA like '%MSIE 6.%' then
            'Internet Explorer 6.x'
        when CLIENT_UA like '%MSIE 5.%' then
            'Internet Explorer 5.x'

        -- Firefox
        when CLIENT_UA like '%Firefox/35.%' then
            'Firefox 35.x'
        when CLIENT_UA like '%Firefox/34.%' then
            'Firefox 34.x'
        when CLIENT_UA like '%Firefox/33.%' then
            'Firefox 33.x'
        when CLIENT_UA like '%Firefox/32.%' then
            'Firefox 32.x'
        when CLIENT_UA like '%Firefox/31.%' then
            'Firefox 31.x'
        when CLIENT_UA like '%Firefox/30.%' then
            'Firefox 30.x'
        when CLIENT_UA like '%Firefox/29.%' then
            'Firefox 29.x'
        when CLIENT_UA like '%Firefox/28.%' then
            'Firefox 28.x'
        when CLIENT_UA like '%Firefox/27.%' then
            'Firefox 27.x'
        when CLIENT_UA like '%Firefox/26.%' then
            'Firefox 26.x'
        when CLIENT_UA like '%Firefox/25.%' then
            'Firefox 25.x'
        when CLIENT_UA like '%Firefox/24.%' then
            'Firefox 24.x'
        when CLIENT_UA like '%Firefox/23.%' then
            'Firefox 23.x'
        when CLIENT_UA like '%Firefox/22.%' then
            'Firefox 22.x'
        when CLIENT_UA like '%Firefox/21.%' then
            'Firefox 21.x'
        when CLIENT_UA like '%Firefox/20.%' then
            'Firefox 20.x'
        when CLIENT_UA like '%Firefox/19.%' then
            'Firefox 19.x'
        when CLIENT_UA like '%Firefox/18.%' then
            'Firefox 18.x'
        when CLIENT_UA like '%Firefox/17.%' then
            'Firefox 17.x'
        when CLIENT_UA like '%Firefox/16.%' then
            'Firefox 16.x'
        when CLIENT_UA like '%Firefox/15.%' then
            'Firefox 15.x'
        when CLIENT_UA like '%Firefox/14.%' then
            'Firefox 14.x'
        when CLIENT_UA like '%Firefox/13.%' then
            'Firefox 13.x'
        when CLIENT_UA like '%Firefox/12.%' then
            'Firefox 12.x'
        when CLIENT_UA like '%Firefox/11.%' then
            'Firefox 11.x'
        when CLIENT_UA like '%Firefox/10.%' then
            'Firefox 10.x'
        when CLIENT_UA like '%Firefox/9.%' then
            'Firefox 9.x'
        when CLIENT_UA like '%Firefox/8.%' then
            'Firefox 8.x'
        when CLIENT_UA like '%Firefox/7.%' then
            'Firefox 7.x'
        when CLIENT_UA like '%Firefox/6.%' then
            'Firefox 6.x'
        when CLIENT_UA like '%Firefox/5.%' then
            'Firefox 5.x'
        when CLIENT_UA like '%Firefox/4.%' then
            'Firefox 4.x'
        when CLIENT_UA like '%Firefox/3.%' then
            'Firefox 3.x'
        when CLIENT_UA like '%Firefox/2.%' then
            'Firefox 2.x'
        when CLIENT_UA like '%Firefox/1.%' then
            'Firefox 1.x'

        -- Chrome
        when CLIENT_UA like '%Chrome%' then
            'Chrome'

        -- Safari
        when CLIENT_UA like '%Safari%' then
            'Safari'

        -- Opera
        when CLIENT_UA like '%Opera 12.%' then
            'Opera 12.x'
        when CLIENT_UA like '%Opera 11.%' then
            'Opera 11.x'
        when CLIENT_UA like '%Opera 10.%' then
            'Opera 10.x'
        when CLIENT_UA like '%Opera 9.%' or CLIENT_UA like '%Opera/9%' then
            'Opera 9.x'
        when CLIENT_UA like '%Opera 8.%' then
            'Opera 8.x'
        when CLIENT_UA like '%Opera 7.%' then
            'Opera 7.x'
        when CLIENT_UA like '%Opera 6.%' then
            'Opera 6.x'
        when CLIENT_UA like '%Opera 3.%' then
            'Opera 3.x'

        -- BOT
        when CLIENT_UA like '%Googlebot%' then
            'Googlebot'
        when CLIENT_UA like '%bingbot%' then
            'bingbot'
        else
            CLIENT_UA
    end as NAME,
    count(*) as COUNT
from
    ACCESS_LOG
where
    ACCESS_PATH = '/'
group by
    NAME
order by
    COUNT desc

