-- リファラ
select
    ACCESS_REFERER as NAME,
    count(*) as COUNT
from
    ACCESS_LOG
where
    length(ACCESS_REFERER) > 0
    and (
        ACCESS_REFERER not like :url
        or
        ACCESS_REFERER not like :www
    )
group by
    NAME
order by
    COUNT desc

