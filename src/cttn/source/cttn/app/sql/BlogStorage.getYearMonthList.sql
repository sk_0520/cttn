--
select
    YEAR,
    MONTH
from
    (
        select
            substr(CMN_CREATE, 1, 4) as YEAR,
            substr(CMN_CREATE, 5, 2) as MONTH
        from
            BLOG_HEAD
        where
            1=1
            {DELETE}
    )
group by
    YEAR,
    MONTH
order by
    YEAR,
    MONTH
