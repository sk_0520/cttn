-- ブラウザ別
select
    case
        when CLIENT_UA like '%MSIE%' or CLIENT_UA like '%Trident%' then
            'Internet Explorer'
        when CLIENT_UA like '%Firefox%' then
            'Firefox'
        when CLIENT_UA like '%Chrome%' then
            'Chrome'
        when CLIENT_UA like '%Safari%' then
            'Safari'
        when CLIENT_UA like '%Opera%' then
            'Opera'
        when CLIENT_UA like '%bot%' then
            'クローラ'
        else
            'Others'
    end as NAME,
    count(*) as COUNT
from
    ACCESS_LOG
where
    ACCESS_PATH = '/'
group by
    NAME
order by
    COUNT desc

