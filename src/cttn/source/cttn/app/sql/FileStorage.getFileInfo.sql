--
select
    FILE_ID,
    FILE_KEY,
    FILE_TITLE,
    FILE_PATH,
    FILE_NAME,
    FILE_SIZE,
    CMN_CREATE,
    CMN_UPDATE,
    CMN_DELETE,
    CMN_REVISION
from
    FILE_INFO
where
    FILE_KEY = :key
    {DELETE}
order by
    CMN_CREATE

