-- OS別
select
    case
        when CLIENT_UA like '%Windows%' then
            'Windows'
        when CLIENT_UA like '%Mac OS X%' or CLIENT_UA like '%MacOS%' then
            'Macintosh'
        when CLIENT_UA like '%Android%' then
            'Android'
        when CLIENT_UA like '%Linux%' then
            'Linux'
        when CLIENT_UA like '%FreeBSD%' then
            'FreeBSD'
        when CLIENT_UA like '%iPhone%' then
            'iPhone'
        when CLIENT_UA like '%bot%' then
            'クローラ'
        else
            CLIENT_UA
    end as NAME,
    count(*) as COUNT
from
    ACCESS_LOG
where
    ACCESS_PATH = '/'
group by
    NAME
order by
    COUNT desc

