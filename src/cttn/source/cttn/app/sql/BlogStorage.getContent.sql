--
select
    BH.BLOG_ID,
    BH.BLOG_TITLE,
    BH.CMN_CREATE,
    BH.CMN_UPDATE,
    BH.CMN_DELETE,
    BH.CMN_REVISION,
    BB.BLOG_CONTENT
from
    BLOG_HEAD BH
inner join
    BLOG_BODY BB
    on BB.BLOG_ID = BH.BLOG_ID
where
    BH.BLOG_ID = :id
    {DELETE}



