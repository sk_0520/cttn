--
select
    ACCESS_PATH  as NAME,
    sum(COUNTER) as COUNT
from
    ACCESS_COUNT
where
    ACCESS_PATH not in (
        '/app/access',
        '/app/blog-edit'
    )
group by
    NAME
order by
    COUNTER desc

