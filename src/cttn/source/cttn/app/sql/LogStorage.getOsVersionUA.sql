-- OSバージョン別
select
    case
        -- Windows
        when CLIENT_UA like '%Windows NT 6.3%' then
            'Windows 8.1'
        when CLIENT_UA like '%Windows NT 6.3%' then
            'Windows 8'
        when CLIENT_UA like '%Windows NT 6.1%' then
            'Windows 7'
        when CLIENT_UA like '%Windows NT 6.0%' then
            'Windows Vista'
        when CLIENT_UA like '%Windows NT 5.2%' then
            'Windows Server 2003'
        when CLIENT_UA like '%Windows NT 5.1%' then
            'Windows XP'
        when CLIENT_UA like '%Windows NT 5.0%' or CLIENT_UA like '%Windows NT 5.01%' then
            'Windows 2000'
        when CLIENT_UA like '%Windows NT 4.0%' then
            'Windows NT 4.0'
        when CLIENT_UA like '%Windows NT 4.0%' then
            'Windows 98'
        when CLIENT_UA like '%Windows 98%' then
            'Windows 95'

        -- Android
        when CLIENT_UA like '%Android 4.%' then
            'Android 4.x'
        when CLIENT_UA like '%Android 3.%' then
            'Android 3.x'
        when CLIENT_UA like '%Android 2.%' then
            'Android 2.x'
        when CLIENT_UA like '%Android 1.%' then
            'Android 1.x'

        when CLIENT_UA like '%bot%' then
            'クローラ'
        else
            CLIENT_UA
    end as NAME,
    count(*) as COUNT
from
    ACCESS_LOG
where
    ACCESS_PATH = '/'
group by
    NAME
order by
    COUNT desc

