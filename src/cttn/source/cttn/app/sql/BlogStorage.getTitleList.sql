--
select
    BLOG_ID,
    BLOG_TITLE,
    CMN_CREATE,
    CMN_UPDATE,
    CMN_DELETE,
    CMN_REVISION
from
    BLOG_HEAD
where
    CMN_CREATE like :likeDate
    {DELETE}
order by
    CMN_CREATE {ORDER}

