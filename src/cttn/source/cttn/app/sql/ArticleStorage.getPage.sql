--
select
    AH.ARTICLE_ID,
    AH.ARTICLE_GROUP_ID,
    AH.ARTICLE_TITLE,
    AB.ARTICLE_CONTENT,
    CMN_CREATE,
    CMN_UPDATE,
    CMN_DELETE,
    CMN_REVISION
from
    ARTICLE_HEAD AH
    inner join ARTICLE_BODY AB using(ARTICLE_ID)
where
    ARTICLE_ID = :id
    {DELETE}
order by
    ARTICLE_SEQ

