--
select
    LINE,
    BLOG_ID,
    BLOG_TITLE,
    CMN_CREATE,
    CMN_UPDATE,
    CMN_DELETE,
    CMN_REVISION,
    BLOG_CONTENT
from
    (
        select
            BH.rowid as LINE,
            BH.BLOG_ID,
            BH.BLOG_TITLE,
            BH.CMN_CREATE,
            BH.CMN_UPDATE,
            BH.CMN_DELETE,
            BH.CMN_REVISION,
            BB.BLOG_CONTENT
        from
            BLOG_HEAD BH
        inner join
            BLOG_BODY BB
            using(BLOG_ID)
        where
            CMN_CREATE like :likeDate
            {DELETE}
        order by
            BH.CMN_CREATE {ORDER}
    )
order by 
    LINE {ORDER}


