--
update
    BLOG_HEAD
set
    CMN_UPDATE   = :timestamp,
    CMN_DELETE   = case 
                    when CMN_DELETE is null then
                        :timestamp
                    else
                        null
                   end,
    CMN_REVISION = CMN_REVISION + 1
where
    ({TARGET_ID})
