/**
記事！
*/
module cttn.app.entity.article_entity;

import cttn.utility;
import cttn.app.entity.entity;
import cttn.app.model.article_base_model;


/**
*/
class ArticleGroupEntity: Entity
{
	mixin CommonColumnBuilder;
	ArticleGroupId groupId;
	string groupName;
	size_t sequence;
	
	override protected void setParam(DBQuery query, QueryType type)
	{
		query.setParam("groupId",   this.groupId);
		query.setParam("groupName", this.groupName);
		query.setParam("sequence",  this.sequence);
		this.cc.setParam(query, type);
	}

	override protected string getInsertCommand()
	{
		return q{
			insert into ARTICLE_GROUP
			(
				ARTICLE_GROUP_ID,
				ARTICLE_GROUP_NAME,
				ARTICLE_GROUP_SEQ,
				CMN_CREATE,
				CMN_UPDATE,
				CMN_DELETE,
				CMN_REVISION
			) values (
				:groupId,
				:groupName,
				:sequence,
				:createTimestamp,
				:updateTimestamp,
				:deleteTimestamp,
				:revision
			)
		};
	}
	override protected string getUpdateCommand()
	{
		return q{
			update
				ARTICLE_GROUP
			set
				ARTICLE_GROUP_NAME = :groupName,
				ARTICLE_GROUP_SEQ  = :sequence,
				CMN_UPDATE         = :updateTimestamp,
				CMN_DELETE         = :deleteTimestamp,
				CMN_REVISION       = CMN_REVISION + 1
			where
				ARTICLE_GROUP_ID   = :groupId
		};
	}
}


class ArticleHeadEntity: Entity
{
	mixin CommonColumnBuilder;
	ArticleId       id;
	ArticleGroupId  groupId;
	string          title;
	size_t          sequence;
	
	override protected void setParam(DBQuery query, QueryType type)
	{
		query.setParam("id",       this.id);
		query.setParam("groupId",  this.groupId);
		query.setParam("title",    this.title);
		query.setParam("sequence", this.sequence);
		
		this.cc.setParam(query, type);
	}
	
	override protected string getInsertCommand()
	{
		return q{
			insert into ARTICLE_HEAD
			(
				ARTICLE_ID,
				ARTICLE_GROUP_ID,
				ARTICLE_TITLE,
				ARTICLE_SEQ,
				CMN_CREATE,
				CMN_UPDATE,
				CMN_DELETE,
				CMN_REVISION
			) values (
				:id,
				:groupId,
				:title,
				:sequence,
				:createTimestamp,
				:updateTimestamp,
				:deleteTimestamp,
				:revision
			)
		};
	}
	override protected string getUpdateCommand()
	{
		return q{
			update
				ARTICLE_HEAD
			set
				ARTICLE_GROUP_ID = :groupId,
				ARTICLE_TITLE    = :title,
				ARTICLE_SEQ      = :sequence,
				CMN_UPDATE = :updateTimestamp,
				CMN_DELETE = :deleteTimestamp,
				CMN_REVISION = CMN_REVISION + 1
			where
				ARTICLE_ID    = :id
		};
	}
}


class ArticleBodyEntity: Entity
{
	ArticleId id;
	string    content;
	
	override protected void setParam(DBQuery query, QueryType type)
	{
		query.setParam("id",      this.id);
		query.setParam("content", this.content);
	}
	
	override protected string getInsertCommand()
	{
		return q{
			insert into ARTICLE_BODY
			(
				ARTICLE_ID,
				ARTICLE_CONTENT
			) values (
				:id,
				:content
			)
		};
	}
	override protected string getUpdateCommand()
	{
		return q{
			update
				ARTICLE_BODY
			set
				ARTICLE_CONTENT = :content
			where
				ARTICLE_ID    = :id
		};
	}
}




