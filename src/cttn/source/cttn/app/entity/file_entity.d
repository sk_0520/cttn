/**
ファイル。
*/
module cttn.app.entity.file_entity;

import std.datetime;

import cttn.utility;
import cttn.app.model.file_model;
import cttn.app.entity.entity;

/**
*/
class FileInfoEntity: Entity
{
	mixin CommonColumnBuilder;
	
	FileId id;
	string key;
	string title;
	string path;
	string name;
	ulong  size;
	
	override protected void setParam(DBQuery query, QueryType type)
	{
		query.setParam("id",    this.id);
		query.setParam("key",   this.key);
		query.setParam("title", this.title);
		query.setParam("path",  this.path);
		query.setParam("name",  this.name);
		query.setParam("size",  this.size);
		this.cc.setParam(query, type);
	}

	override protected string getInsertCommand()
	{
		return q{
			insert into FILE_INFO
			(
				FILE_ID,
				FILE_KEY,
				FILE_TITLE,
				FILE_PATH,
				FILE_NAME,
				FILE_SIZE,
				CMN_CREATE,
				CMN_UPDATE,
				CMN_DELETE,
				CMN_REVISION
			) values (
				:id,
				:key,
				:title,
				:path,
				:name,
				:size,
				:createTimestamp,
				:updateTimestamp,
				:deleteTimestamp,
				:revision
			)
		};
	}

	override protected string getUpdateCommand()
	{
		return q{
			update
				FILE_INFO
			set
				FILE_KEY   = :key
				FILE_TITLE = :title
				FILE_PATH  = :path,
				FILE_NAME  = :name,
				FILE_SIZE  = :size,
				CMN_UPDATE = :updateTimestamp,
				CMN_DELETE = :deleteTimestamp,
				CMN_REVISION = CMN_REVISION + 1
			where
				FILE_ID    = :id
		};
	}
}


