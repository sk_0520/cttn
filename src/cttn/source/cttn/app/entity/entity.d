/**
エンティティ。
*/
module cttn.app.entity.entity;

import std.datetime;

public import cttn.utility;
public import cttn.dbmanager;


enum QueryType
{
	insert,
	update,
	remove,
}


/**
*/
abstract class Entity
{
	protected abstract void setParam(DBQuery query, QueryType type);
	
	protected string getInsertCommand() { assert(false); }
	protected string getUpdateCommand() { assert(false); }
	protected string getDeleteCommand() { assert(false); }
	
	void executeInsert(DBManager db)
	{
		auto command = getInsertCommand();
		auto query = db.createQuery(command);
		setParam(query, QueryType.insert);
		query.execute();
	}
	void executeUpdate(DBManager db)
	{
		auto command = getUpdateCommand();
		auto query = db.createQuery(command);
		setParam(query, QueryType.update);
		query.execute();
	}
	void executeDelete(DBManager db)
	{
		auto command = getDeleteCommand();
		auto query = db.createQuery(command);
		setParam(query, QueryType.remove);
		query.execute();
	}
}

class CommonColumn
{
	SysTime  createTimestamp;
	SysTime  updateTimestamp;
	SysTime* deleteTimestamp;
	private size_t _revision;
	
	@property
	size_t revision() const
	{
		return this._revision;
	}

	void setDelete(const ref SysTime time)
	{
		this.deleteTimestamp = new SysTime;
		*this.deleteTimestamp = time;
	}
	void unsetDelete()
	{
		delete this.deleteTimestamp;
	}
	@property
	bool isDeleted() const
	{
		return this.deleteTimestamp !is null;
	}

	void setParam(DBQuery query, QueryType type)
	{
		if(type == QueryType.insert) {
			query.setParam("revision",        1);
			query.setParam("createTimestamp", this.createTimestamp);
		}
		query.setParam("updateTimestamp", this.updateTimestamp);
		if(this.deleteTimestamp) {
			query.setParam("deleteTimestamp", *this.deleteTimestamp);
		} else {
			query.setParam("deleteTimestamp", null);
		}
	}

	private void setTimestamp(const ref SysTime time)
	{
		this.createTimestamp = time;
		this.updateTimestamp = time;
	}
	void setTimestampNow()
	{
		auto time = getNowTime();
		setTimestamp(time);
	}
}

mixin template CommonColumnBuilder()
{
	CommonColumn cc = new CommonColumn();
}
