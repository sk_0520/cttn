/**
ログ
*/
module cttn.app.entity.log_entity;

import std.datetime;

import cttn.utility;
import cttn.app.entity.entity;


/**
*/
class AccessCountEntity: Entity
{
	string  accessPath;
	SysTime loggingTime;
	string  clientIpAddress;
	//size_t  counter;
	
	override protected void setParam(DBQuery query, QueryType type)
	{
		query.setParam("accessPath",      this.accessPath);
		query.setParam("loggingTime",     this.loggingTime);
		query.setParam("clientIpAddress", this.clientIpAddress);
		//if(type == QueryType.insert) {
		//	query.setParam("counter", this.counter);
		//}
	}

	override protected string getInsertCommand()
	{
		return q{
			insert into ACCESS_COUNT
			(
				ACCESS_PATH,
				LOGGING_TIME,
				CLIENT_IPADDR,
				COUNTER
			) values (
				:accessPath,
				:loggingTime,
				:clientIpAddress,
				1
			)
		};
	}

	override protected string getUpdateCommand()
	{
		return q{
			update
				ACCESS_COUNT
			set
				LOGGING_TIME  = :loggingTime,
				CLIENT_IPADDR = :clientIpAddress,
				COUNTER       = COUNTER + 1
			where
				ACCESS_PATH   = :accessPath
		};
	}
}


/**
*/
class AccessLogEntity: Entity
{
	SysTime loggingTime;
	string clientIpAddress;
	string clientHost;
	string clientUserAgent;
	string accessPath;
	string accessRequest;
	string accessReferer;

	
	override protected void setParam(DBQuery query, QueryType type)
	{
		query.setParam("loggingTime",     this.loggingTime);
		query.setParam("clientIpAddress", this.clientIpAddress);
		query.setParam("clientHost",      this.clientHost);
		query.setParam("clientUserAgent", this.clientUserAgent);
		query.setParam("accessPath",      this.accessPath);
		query.setParam("accessRequest",   this.accessRequest);
		query.setParam("accessReferer",   this.accessReferer);
	}
	
	override protected string getInsertCommand()
	{
		return q{
			insert into ACCESS_LOG
			(
				LOGGING_TIME,
				CLIENT_IPADDR,
				CLIENT_HOST,
				CLIENT_UA,
				ACCESS_PATH,
				ACCESS_REQUEST,
				ACCESS_REFERER
			) values (
				:loggingTime,
				:clientIpAddress,
				:clientHost,
				:clientUserAgent,
				:accessPath,
				:accessRequest,
				:accessReferer
			)
		};
	}
}

/**
*/
class ErrorLogEntity: Entity
{
	SysTime loggingTime;
	string clientIpAddress;
	string clientHost;
	string clientUserAgent;
	
	int    errorCode;
	string errorMessage;
	
	override protected void setParam(DBQuery query, QueryType type)
	{
		query.setParam("loggingTime",     this.loggingTime);
		query.setParam("clientIpAddress", this.clientIpAddress);
		query.setParam("clientHost",      this.clientHost);
		query.setParam("clientUserAgent", this.clientUserAgent);
		query.setParam("errorCode",       this.errorCode);
		query.setParam("errorMessage",    this.errorMessage);
	}
	override protected string getInsertCommand()
	{
		return q{
			insert into ERROR_LOG
			(
				LOGGING_TIME,
				CLIENT_IPADDR,
				CLIENT_HOST,
				CLIENT_UA,
				ERROR_CODE,
				ERROR_MSG
			) values (
				:loggingTime,
				:clientIpAddress,
				:clientHost,
				:clientUserAgent,
				:errorCode,
				:errorMessage
			)
		};
	}


}
