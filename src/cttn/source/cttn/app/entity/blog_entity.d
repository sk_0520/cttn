/**
ブログ！
*/
module cttn.app.entity.blog_entity;

import cttn.utility;
import cttn.app.entity.entity;
import cttn.app.model.blog_base_model;


/**
*/
class BlogHeadEntity: Entity
{
	mixin CommonColumnBuilder;
	BlogId id;
	string title;
	
	override protected void setParam(DBQuery query, QueryType type)
	{
		query.setParam("id",    this.id);
		query.setParam("title", this.title);
		this.cc.setParam(query, type);
	}

	override protected string getInsertCommand()
	{
		return q{
			insert into BLOG_HEAD
			(
				BLOG_ID,
				BLOG_TITLE,
				CMN_CREATE,
				CMN_UPDATE,
				CMN_DELETE,
				CMN_REVISION
			) values (
				:id,
				:title,
				:createTimestamp,
				:updateTimestamp,
				:deleteTimestamp,
				:revision
			)
		};
	}
	override protected string getUpdateCommand()
	{
		return q{
			update
				BLOG_HEAD
			set
				BLOG_TITLE = :title,
				CMN_UPDATE = :updateTimestamp,
				CMN_DELETE = :deleteTimestamp,
				CMN_REVISION = CMN_REVISION + 1
			where
				BLOG_ID    = :id
		};
	}
}


/**
*/
class BlogBodyEntity: Entity
{
	BlogId   id;
	string   content;
	
	override protected void setParam(DBQuery query, QueryType type)
	{
		query.setParam("id",      this.id);
		query.setParam("content", this.content);
	}
	
	override protected string getInsertCommand()
	{
		return q{
			insert into BLOG_BODY
			(
				BLOG_ID,
				BLOG_CONTENT
			) values (
				:id,
				:content
			)
		};
	}
	override protected string getUpdateCommand()
	{
		return q{
			update
				BLOG_BODY
			set
				BLOG_CONTENT = :content
			where
				BLOG_ID    = :id
		};
	}
}

/**
*/
class BlogTagEntity: Entity
{
	BlogId   id;
	string   name;
	
	override protected void setParam(DBQuery query, QueryType type)
	{
		query.setParam("id",      this.id);
		query.setParam("name",    this.name);
	}
	override protected string getInsertCommand()
	{
		return q{
			insert into BLOG_TAG
			(
				BLOG_ID,
				BLOG_TAG_NAME
			) values (
				:id,
				:name
			)
		};
	}
}

