/**
記事編集。
*/
module cttn.app.model.article_edit_model;

import vibe.d;

import cttn.utility;
import cttn.mvc;
public import cttn.app.model.article_base_model;
import cttn.app.dto.article_dto;

/**
*/
class ArticleEditModel: ArticleBaseModel
{
	string targetGroupName;

	string targetTitle;
	string targetContent;

	ArticleGroupId[] switchGroupTargets;
	ArticleId[] switchPageTargets;
	
	bool errorTargetGroupName;
	
	bool errorTargetTitle;
	bool errorTargetContent;

	bool errorSwitchGroupTargetsNoCheck;
	bool errorSwitchPageTargetsNoCheck;

	SysTime createTimestamp;
	
	override void importRequest(in HTTPServerRequest req)
	{
		convertValue(req, "targetGroupName", this.targetGroupName);
		
		convertValue(req, "targetTitle",     this.targetTitle);
		convertValue(req, "targetContent",   this.targetContent);
		
		convertValue(req, "switchGroupTargets", this.switchGroupTargets);
		convertValue(req, "switchPageTargets",  this.switchPageTargets);
		
		super.importRequest(req);
	}
}


