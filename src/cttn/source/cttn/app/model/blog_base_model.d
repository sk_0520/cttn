/**
最初のページ。
*/
module cttn.app.model.blog_base_model;

import vibe.d;

import cttn.utility;
import cttn.mvc;
import cttn.app.dto.blog_dto;

/**
*/
enum BlogId
{
	init = 0,
}

struct BlogContent
{
	alias dto this;
	BlogContentDto dto;
	string markdownContent;
}

/**
*/
abstract class BlogBaseModel: CttnModel
{
	BlogId targetId;

	BlogTitleDto[]   titleList;
	BlogContent[] contentList;
	string[][BlogId] contentsTags;
	
	string[] allTags;

	ushort[][ushort] yearMonth;

	ushort year;
	ushort month;
	bool isSafeYear;
	bool isSafeMonth;
	
	bool orderByAsc;
	@property
	string order() const
	{
		return this.orderByAsc ? "asc": "desc";
	}

	override void importRequest(in HTTPServerRequest req)
	{
		super.importRequest(req);
		
		convertValue(req, "targetId", this.targetId);
		
		if(!convertValue(req, "year", this.year)) {
			this.isSafeYear = this.year <= 9999;
		}
		if(!convertValue(req, "month", this.month)) {
			this.isSafeMonth = 1 <= this.month && this.month <= 12;
		}

		string order;
		convertValue(req, "order", order);
		this.orderByAsc = order == "asc";
	}
}


