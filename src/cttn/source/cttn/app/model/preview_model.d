/**
マークダウンのプレビュー。
*/
module cttn.app.model.preview_model;

import vibe.d;

import cttn.mvc;


/**
*/
class PreviewModel: CttnModel
{
	string contentName;
	string contentBody;
	bool   inDownload;

	string contentMarkdown;
	
	override void importRequest(in HTTPServerRequest req)
	{
		super.importRequest(req);
		
		convertValue(req, "contentName", this.contentName);
		convertValue(req, "contentBody", this.contentBody);
		convertValue(req, "inDownload",  this.inDownload);
	}
}


