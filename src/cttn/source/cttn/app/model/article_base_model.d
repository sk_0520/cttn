/**
記事。
*/
module cttn.app.model.article_base_model;

import vibe.d;

import cttn.utility;
import cttn.mvc;
import cttn.app.dto.article_dto;

/**
*/
enum ArticleGroupId
{
	init = 0,
}
/**
*/
enum ArticleId
{
	init = 0,
}


enum ArticleMode
{
	groupEdit   = "group-edit",
	groupSwicth = "group-switch",
	pageEdit    = "page-edit",
	pageSwitch  = "page-switch",
}

/**
*/
abstract class ArticleBaseModel: CttnModel
{
	ArticleGroupId targetGroupId;
	ArticleId targetId;
	ArticleMode mode;
	
	ArticleGroupDto[] groupList;
	ArticleTitleDto[][ArticleGroupId] titleList;

	override void importRequest(in HTTPServerRequest req)
	{
		super.importRequest(req);
		
		convertValue(req, "targetGroupId", this.targetGroupId);
		convertValue(req, "targetId",      this.targetId);
		convertValue(req, "mode",          this.mode);
	}
}


