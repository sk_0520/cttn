/**
ファイル。
*/
module cttn.app.model.file_model;

import std.string;

import vibe.d;

import cttn.mvc;
import cttn.app.dto.file_dto;


enum FileId
{
	init = 0
}


/**
*/
class FileModel: CttnModel
{
	FileId targetId;
	bool   save;
	bool   thumbnail;
	
	string targetKey;
	
	FileInfoDto[][string] fileInfoList;

	FileId[] switchTargets;
	
	override void importRequest(in HTTPServerRequest req)
	{
		super.importRequest(req);
		
		convertValue(req, "targetId",      this.targetId);
		convertValue(req, "save",          this.save);
		convertValue(req, "thumbnail",     this.thumbnail);
		convertValue(req, "targetKey",     this.targetKey);
		convertValue(req, "switchTargets", this.switchTargets);
		this.targetKey = this.targetKey.strip();
	}
}


