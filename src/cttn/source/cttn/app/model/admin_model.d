/**
管理者。
*/
module cttn.app.model.admin_model;

import vibe.d;

import cttn.mvc;


enum LoginState
{
	none,
	failure,
	success,
}

/**
*/
class AdminModel: CttnModel
{
	LoginState state;
	
	string loginName;
	string loginPass;

	override void importRequest(in HTTPServerRequest req)
	{
		super.importRequest(req);
		
		convertValue(req, "loginName", this.loginName);
		convertValue(req, "loginPass", this.loginPass);
	}
}


