/**
記事表示。
*/
module cttn.app.model.article_view_model;

import vibe.d;

import cttn.utility;
import cttn.mvc;
public import cttn.app.model.article_base_model;
import cttn.app.dto.article_dto;

struct ArticlePage
{
	alias dto this;
	ArticlePageDto dto;
	string markdownContent;
}

/**
*/
class ArticleViewModel: ArticleBaseModel
{
	ArticleGroupDto   viewGroup;
	ArticleTitleDto[] viewTitleList;
	
	ArticlePage viewPage;
	
	override void importRequest(in HTTPServerRequest req)
	{
		super.importRequest(req);
	}
}


