/**
最初のページ。
*/
module cttn.app.model.blog_edit_model;

import std.algorithm;
import std.array;
import std.datetime;

import vibe.d;

import cttn.mvc;
public import cttn.app.model.blog_base_model;

enum BlogEditMode
{
	create = "create",
	target = "target",
}

/**
*/
class BlogEditModel: BlogBaseModel
{

	BlogEditMode editMode;
	string targetTitle;
	string targetContent;
	
	BlogId[] switchTargets;
	string[] targetTags;

	string editMessage;
	
	bool errorTargetTitle;
	bool errorTargetContent;
	string[] errorTargetTags;
	bool errorSwitchTargetsNoCheck;
	
	SysTime createTimestamp;
	
	override void importRequest(in HTTPServerRequest req)
	{
		super.importRequest(req);
		
		convertValue(req, "editMode", this.editMode);
		
		convertValue(req, "targetTitle",   this.targetTitle);
		convertValue(req, "targetContent", this.targetContent);
		
		convertValue(req, "switchTargets", this.switchTargets);
		
		convertValue(req, "targetTags", this.targetTags);
		this.targetTags = this.targetTags.sort().array().filter!(s => s.length).uniq().array();
	}
}


