/**
ブログ表示。
*/
module cttn.app.model.blog_view_model;

import std.algorithm;
import std.array;
import std.datetime;

import vibe.d;

import cttn.literal;
import cttn.mvc;
public import cttn.app.model.blog_base_model;

enum ViewType
{
	normal,
	tag,
	archive,
	single,
}

/**
*/
class BlogViewModel: BlogBaseModel
{
	ViewType viewType;
	size_t offsetPage;
	size_t limitPage;
	string targetTag;

	bool future;
	bool old;
	
	
	override void importRequest(in HTTPServerRequest req)
	{
		super.importRequest(req);
		
		convertValue(req, "viewType", this.viewType);
		convertValue(req, "offsetPage", this.offsetPage);
		convertValue(req, "limitPage", this.limitPage);
		convertValue(req, "targetTag", this.targetTag);
		
		if(!this.limitPage) {
			this.limitPage = Literal.blogLimitPage;
		}
	}
}


