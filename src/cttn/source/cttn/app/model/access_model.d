/**
アクセス情報。
*/
module cttn.app.model.access_model;

import cttn.mvc;
import cttn.app.dto.log_dto;


/**
*/
struct CountData
{
	string name;
	size_t count;
	real   percent;
}

struct Count
{
	CountData[] data;
	size_t total;

	alias data this;

	/**
	Bugs:
		std.algorithm使うと Linux DMD が吹っ飛ぶ。
	*/
	this(in LogCountDto[] list)
	{
		data.length = list.length;
		foreach(i; 0 .. list.length) {
			this.data[i].name = list[i].name;
			this.total += this.data[i].count = list[i].count;
		}
		foreach(i; 0 .. this.data.length) {
			this.data[i].percent = (cast(real)(this.data[i].count) / cast(real)this.total) * cast(real)100;
		}
	}
}


/**
*/
class AccessModel: CttnModel
{
	Count pageCount;
	Count errorCount;

	Count browserTotalUA;
	Count browserVersionUA;
	Count osTotalUA;
	Count osVersionUA;
	
	Count referer;
}


