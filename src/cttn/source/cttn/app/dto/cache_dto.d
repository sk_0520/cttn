/**
キャッシュ関係。
*/
module cttn.app.dto.cache_dto;

import std.datetime;

import d2sqlite3;

import cttn.app.dto.dto;


/**
*/
class LoginDto: Dto
{
	string  key;
	string  name;
	SysTime access;
	
	override void importRow_Impl(ref QueryCache.CachedRow row)
	{
		importFromDBValue(row["KEY"], this.key);
		importFromDBValue(row["NAME"], this.name);
		importFromDBValue(row["ACCESS"], this.access);
	}
}

