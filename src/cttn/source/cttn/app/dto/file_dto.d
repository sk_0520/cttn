/**
ファイル。
*/
module cttn.app.dto.file_dto;

import std.datetime;

import d2sqlite3;

import cttn.type;
import cttn.app.dto.dto;
import cttn.app.model.file_model;


class FileKeyDto: Dto
{
	string key;
	
	override void importRow_Impl(ref QueryCache.CachedRow row)
	{
		importFromDBValue(row["FILE_KEY"], this.key);
	}
}

class FileInfoDto: Dto, ICommonColumnDto
{
	mixin CommonColumnDtoBuilder;
	FileId id;
	string key;
	string title;
	string path;
	string name;
	ulong  size;
	
	override void importRow_Impl(ref QueryCache.CachedRow row)
	{
		importFromDBValue(row["FILE_ID"],    this.id);
		importFromDBValue(row["FILE_KEY"],   this.key);
		importFromDBValue(row["FILE_TITLE"], this.title);
		importFromDBValue(row["FILE_PATH"],  this.path);
		importFromDBValue(row["FILE_NAME"],  this.name);
		importFromDBValue(row["FILE_SIZE"],  this.size);

		importCCRow(row);
	}
}

class FileDownloadDto: Dto
{
	bool   enable;
	string title;
	string path;
	string name;
	ulong  size;
	
	override void importRow_Impl(ref QueryCache.CachedRow row)
	{
		importFromDBValue(row["FILE_TITLE"], this.title);
		importFromDBValue(row["FILE_PATH"],  this.path);
		importFromDBValue(row["FILE_NAME"],  this.name);
		importFromDBValue(row["FILE_SIZE"],  this.size);
	}
}



