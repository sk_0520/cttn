/**
ログ
*/
module cttn.app.dto.log_dto;

import std.datetime;

import d2sqlite3;

import cttn.type;
import cttn.app.dto.dto;


class LogCountDto: Dto
{
	string name;
	size_t count;
	
	override void importRow_Impl(ref QueryCache.CachedRow row)
	{
		importFromDBValue(row["NAME"],  this.name);
		importFromDBValue(row["COUNT"], this.count);
	}
}




