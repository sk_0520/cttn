/**
ブログ関係。
*/
module cttn.app.dto.blog_dto;

import std.datetime;

import d2sqlite3;

import cttn.type;
import cttn.app.dto.dto;
import cttn.app.model.blog_base_model;


/**
*/
class BlogSimpleTagDto: Dto
{
	string name;
	
	override void importRow_Impl(ref QueryCache.CachedRow row)
	{
		importFromDBValue(row["BLOG_TAG_NAME"], this.name);
	}
}
class BlogTagDto: BlogSimpleTagDto
{
	BlogId id;
	
	override void importRow_Impl(ref QueryCache.CachedRow row)
	{
		super.importRow_Impl(row);
		
		importFromDBValue(row["BLOG_ID"], this.id);
	}
}

class BlogTitleDto: Dto, ICommonColumnDto
{
	mixin CommonColumnDtoBuilder;
	
	BlogId   id;
	string   title;
	
	override void importRow_Impl(ref QueryCache.CachedRow row)
	{
		importFromDBValue(row["BLOG_ID"], this.id);
		importFromDBValue(row["BLOG_TITLE"], this.title);
		importCCRow(row);
	}
}

/**
*/
class BlogContentDto: BlogTitleDto
{
	string   content;
	
	override void importRow_Impl(ref QueryCache.CachedRow row)
	{
		super.importRow_Impl(row);
		
		importFromDBValue(row["BLOG_CONTENT"], this.content);
	}
}

class BlogYearMonthDto: Dto
{
	ushort year;
	ushort month;
	
	override void importRow_Impl(ref QueryCache.CachedRow row)
	{
		importFromDBValue(row["YEAR"],  this.year);
		importFromDBValue(row["MONTH"], this.month);
	}
}






