/**
データトランスファーオブジェクト。
*/
module cttn.app.dto.dto;

import std.datetime;

import d2sqlite3;

import cttn.type;

/**
*/
abstract class Dto
{
	protected abstract void importRow_Impl(ref QueryCache.CachedRow row);

	protected bool importFromDBValue(T)(ColumnData column, ref T value) const
	{
		try {
			static if(is(T == SysTime)) {
				value = dbValueToSysTime(column.as!string);
			} else static if(is(T == enum)) {
				value = fromOriginalValue!T(column);
			} else {
				value = column.as!T;
			}
			return true;
		} catch(Exception ex) {
			return false;
		}
	}

	this()
	{
		clear();
	}

	void clear()
	{
		foreach(ref var; this.tupleof) {
			var = var.init;
		}
	}

	final void importRow(ref QueryCache.CachedRow row)
	{
		clear();
		importRow_Impl(row);
	}
}


interface ICommonColumnDto
{
	@property
	bool isDeleted() const;
	ref const(SysTime) deleteTimestamp() const;
	void importCCRow(ref QueryCache.CachedRow row);
}


/***/
mixin template CommonColumnDtoBuilder()
{
	SysTime  createTimestamp;
	SysTime  updateTimestamp;
	protected SysTime* _deleteTimestamp;
	
	@property
	override bool isDeleted() const
	{
		return this._deleteTimestamp !is null;
	}
	@property
	override ref const(SysTime) deleteTimestamp() const
	{
		return *this._deleteTimestamp;
	}
	size_t   revision;
	
	override void importCCRow(ref QueryCache.CachedRow row)
	{
		importFromDBValue(row["CMN_CREATE"], this.createTimestamp);
		importFromDBValue(row["CMN_UPDATE"], this.updateTimestamp);
		SysTime cmnDelete;
		if(importFromDBValue(row["CMN_DELETE"], cmnDelete)) {
			this._deleteTimestamp = new SysTime();
			*this._deleteTimestamp = cmnDelete;
		} else {
			this._deleteTimestamp = null;
		}
		importFromDBValue(row["CMN_REVISION"], this.revision);
	}
}

