/**
記事。
*/
module cttn.app.dto.article_dto;

import std.datetime;

import d2sqlite3;

import cttn.type;
import cttn.app.dto.dto;
import cttn.app.model.article_base_model;


class ArticleGroupDto: Dto, ICommonColumnDto
{
	mixin CommonColumnDtoBuilder;
	
	ArticleGroupId groupId;
	string groupName;
	size_t sequence;
	
	protected override void importRow_Impl(ref QueryCache.CachedRow row)
	{
		importFromDBValue(row["ARTICLE_GROUP_ID"],   this.groupId);
		importFromDBValue(row["ARTICLE_GROUP_NAME"], this.groupName);
		importFromDBValue(row["ARTICLE_GROUP_SEQ"],  this.sequence);
		
		importCCRow(row);
	}
}

class ArticleTitleDto: Dto, ICommonColumnDto
{
	mixin CommonColumnDtoBuilder;
	
	ArticleId id;
	ArticleGroupId groupId;
	string title;
	size_t sequence;
	
	protected override void importRow_Impl(ref QueryCache.CachedRow row)
	{
		importFromDBValue(row["ARTICLE_ID"],       this.id);
		importFromDBValue(row["ARTICLE_GROUP_ID"], this.groupId);
		importFromDBValue(row["ARTICLE_TITLE"],    this.title);
		importFromDBValue(row["ARTICLE_SEQ"],      this.sequence);
		
		importCCRow(row);
	}
}

class ArticlePageDto: Dto, ICommonColumnDto
{
	mixin CommonColumnDtoBuilder;
	
	ArticleId id;
	ArticleGroupId groupId;
	string title;
	string content;
	
	protected override void importRow_Impl(ref QueryCache.CachedRow row)
	{
		importFromDBValue(row["ARTICLE_ID"],       this.id);
		importFromDBValue(row["ARTICLE_GROUP_ID"], this.groupId);
		importFromDBValue(row["ARTICLE_TITLE"],    this.title);
		importFromDBValue(row["ARTICLE_CONTENT"],  this.content);
		
		importCCRow(row);
	}
}

class ArtcleMoveList: Dto
{
	ArticleId nextId;
	string    nextTitle;
	ArticleId prevId;
	string    prevTitle;
	
	protected override void importRow_Impl(ref QueryCache.CachedRow row)
	{
		importFromDBValue(row["NEXT_ID"],    this.nextId);
		importFromDBValue(row["NEXT_TITLE"], this.nextTitle);
		importFromDBValue(row["PREV_ID"],    this.prevId);
		importFromDBValue(row["PREV_TITLE"], this.prevTitle);
	}
}

