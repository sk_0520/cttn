/**
マークダウン処理拡張。
*/
module cttn.app.logic.markdown;

import std.algorithm;
import std.array;
static import std.ascii;
import std.string;
import std.regex;
//import std.datetime;
//import std.digest.crc;

import vibe.d;

import cttn.type;
import cttn.literal;
import cttn.utility;
import cttn.app.model.file_model;
import cttn.app.dto.file_dto;
import cttn.app.storage.file_storage;
import cttn.app.logic.file;
import cttn.app.logic.image;
import cttn.app.logic.tagelement;

/**
内部ファイルリンク。
*/
private string replaceFileLink(string src, bool scanDelete, FileStorage storage)
in
{
	assert(storage);
}
body
{
	// マークダウン中の内部ダウンロード指定をマークダウン記法へ置き換え
	auto downloadReg = ctRegex!(`<([0-9]+)>`);
	FileId[size_t] donwloadPair;
	size_t count = size_t.max;
	string download(Captures!(string) m)
	{
		FileId fileId;
		convertFromString!(FileId)(m[1], fileId);
		
		auto dto = storage.getDownload(fileId, scanDelete);
		if(dto.enable) {
			donwloadPair[count] = fileId;
			return format("[%s (size: %s)][%s]", dto.title, toSizeString(dto.size), count--);
		} else {
			return " +++ File is invalid +++ ";
		}
	}
	auto replaceDown = replaceAll!(download)(src, downloadReg);
	auto linkList = new string[donwloadPair.length];
	size_t i = 0;
	foreach(k, v; donwloadPair) {
		auto s = format("  [%s]: /app/file?event=download&targetId=%s&thumbnail=true", k, v.toOriginalValue());
		linkList[i++] = s;
	}
	auto links = std.ascii.newline ~ linkList.join(std.ascii.newline);
	auto attachLink = replaceDown ~ std.ascii.newline ~ links;

	return attachLink;
}

/**
言語指定コードをよしなにする。
*/
private string replaceCode(string src)
{
	auto codeHeadReg = ctRegex!(`<pre\s+class="prettyprint"><code>\[\[(.+)\]\]\s*[\r\n]`);
	auto codeHeadContent = replaceAll!(cap => "<pre class='prettyprint brush: "  ~ cap[1].toLower() ~ "'>")(src, codeHeadReg);
	auto codeHeadReg2 = ctRegex!(`(<pre\s+class="prettyprint">)<code>`);
	auto codeHeadContent2 = replaceAll(codeHeadContent, codeHeadReg2, "$1");
	auto codeTailReg = ctRegex!(`</code></pre>`);
	auto codeTailContent = replaceAll(codeHeadContent2, codeTailReg, "</pre>");
	
	return codeTailContent;
}

/**
インラインのマークダウンを拡張。
*/
private string replaceInlineEx(string src)
{
	// 削除
	auto deleteReg = ctRegex!(`~~(.*?)~~`);
	auto deleteContent = replaceAll(src, deleteReg, "<del>$1</del>");
	
	return deleteContent;
}

private string toTable_Impl(string[] lines)
in
{
	assert(lines.length >= 2);
}
body
{
	enum ColumnPos
	{
		none,
		left,
		center,
		right,
	}
	ColumnPos toColumnPos(string s)
	{
		if(s.length < 1) {
			return ColumnPos.none;
		}
		auto head = s[0] == ':';
		auto tail = s[$ - 1] == ':';
		if(head && tail) {
			return ColumnPos.center;
		}
		if(head) {
			return ColumnPos.left;
		}
		if(tail) {
			return ColumnPos.right;
		}
		return ColumnPos.none;
	}

	ColumnPos[] columnPos;

	string[] toCellList(string s)
	{
		auto line = s.strip();
		if(line.length > 2) {
			if(line[0] == '|') {
				line = line[1 .. $];
			}
			if(line[$ - 1] == '|') {
				line = line[0 .. $ - 1];
			}
			line = line.strip();
		}
		return line.split('|');
	}

	// ヘッダ定義は存在するか, 存在すれば方向定義しとく。
	auto hasHeader = false;
	if(lines[1].canFind("---")) {
		auto headerCells = toCellList(lines[1]);
		foreach(cell; headerCells) {
			columnPos ~= toColumnPos(cell);
		}
		hasHeader = true;
	}
	
	// 構築
	auto table = new RootElement("table");
	TagElement tbody;
	foreach(i, line; lines) {
		auto isHeader = i == 0 && hasHeader;
		if(i == 1 && hasHeader) {
			continue;
		}
		TagElement tr;
		if(isHeader) {
			// ヘッダ
			auto thead = table.createTagElement("thead");
			tr = thead.createTagElement("tr");
		} else if(!tbody) {
			tbody = table.createTagElement("tbody");
		}
		if(tbody) {
			tr = tbody.createTagElement("tr");
		}
		assert(tr);

		// セル構築
		foreach(j, cellValue; toCellList(line)) {
			auto tagName = isHeader ? "th": "td";
			auto cell = tr.createTagElement(tagName);
			if(!isHeader) {
				if(j < columnPos.length) {
					auto pos = columnPos[j];
					if(pos != ColumnPos.none) {
						auto posMap = [
							ColumnPos.left:   "left",
							ColumnPos.center: "center",
							ColumnPos.right:  "right",
						];
						cell.setAttribute("class", posMap[pos]);
					}
				}
			}
			cell.createTextElement(cellValue, false);
		}
	}

	//dprint(table);

	return table.toString();
}
unittest
{
	toTable_Impl(["|A|B|C|", "|---|---|---|"]);
	toTable_Impl(["|A|B|C|", "|---|---|---|", "|a|b|c|"]);
	toTable_Impl(["|A|B|C|", "|:---|:---:|---:|", "|a|b|c|"]);
	toTable_Impl(["|A|B|C|", "|a|b|c|"]);
	toTable_Impl(["|A|B|C", "|a|b|c"]);
	toTable_Impl(["A|B|C", "a|b|c"]);
}

/**
ブロックのマークダウンを拡張。
*/
private string replaceBlockEx(string src)
{
	// テーブル
	string toTable(Captures!(string) m)
	{
		auto s = m[1];
		auto lines = s.splitLines();
		assert(lines.length >= 2);

		return toTable_Impl(lines);
	}
	auto tableReg = ctRegex!(`<p>\s{0,3}(\|.*\|[\r\n]\s{0,3}\|[\s\S]*?)</p>`);
	auto tableContent = replaceAll!(toTable)(src, tableReg);

	
	auto quotesReg = ctRegex!(`<p>\s{0,3}"""[\r\n\s]*</p>[\r\n]*([\s\S]*?)<p>\s{0,3}"""[\r\n\s]*</p>`);
	auto quotesContent = replaceAll(tableContent, quotesReg, "<blockquote>$1</blockquote>");
	
	return quotesContent;
}

/**
各種調整
*/
private string replaceAdjust(string src)
{
	// h1-2 を h3 へ落しとく
	auto headingReg = ctRegex!(`<(/?)h[1-2]>`);
	auto headingContent = replaceAll(src, headingReg, "<$1h3>");

	return headingContent;
}

string replaceMarkdown(string src, bool inDownload, bool scanDelete, FileStorage storage)
{
	string plain = src;
	if(inDownload) {
		plain = replaceFileLink(plain, scanDelete, storage);
	}
	
	// マークダウンを変換
	auto markdownSetting = new MarkdownSettings();
	markdownSetting.flags = MarkdownFlags.backtickCodeBlocks | MarkdownFlags.noInlineHtml;
	auto htmlContent = filterMarkdown(plain, markdownSetting);

	//*
	auto codeContent = replaceCode(htmlContent);
	auto inlineContent = replaceInlineEx(codeContent);
	auto blockContent = replaceBlockEx(inlineContent);

	auto adjustContent = replaceAdjust(blockContent);

	return adjustContent;
	//*/
	//return htmlContent;
}



