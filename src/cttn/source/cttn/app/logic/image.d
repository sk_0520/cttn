﻿/**
イメージマジックコマンド。

ラッパーはよ。
*/
module cttn.app.logic.image;

import std.algorithm;
import std.string;
import std.path;
import std.process;

bool isImageExt(string path)
{
	auto dotExt = path.extension();
	if(dotExt.length) {
		auto ext = dotExt[1 .. $].toLower();
		auto list = ["bmp", "png", "jpeg", "jpg", "gif", ];
		return canFind(list, ext);
	}
	return false;
}
unittest
{
	assert(isImageExt("a.bmp"));
	assert(isImageExt("a.png"));
	assert(isImageExt("a.jpeg"));
	assert(isImageExt("a.jpg"));
	assert(isImageExt("a.gif"));
	assert(isImageExt("a.BMP"));
	assert(isImageExt("a.PNG"));
	assert(isImageExt("a.JPEG"));
	assert(isImageExt("a.JPG"));
	assert(isImageExt("a.GIF"));
	assert(!isImageExt("a.ani"));
	assert(!isImageExt("a.svg"));
	assert(!isImageExt("a.tga"));
}

string toThumbnailPath(string path, string suffix) {
	auto dotExt = extension(path);
	return path.setExtension(suffix ~ dotExt);
}

struct ImageMagic
{
static:
	/**
	*/
	void stripExif(string inputPath, string outputPath)
	{
		system(format("convert -strip -auto-orient %s %s", inputPath, outputPath));
	}
	///
	void stripExif(string path)
	{
		system(format("mogrify -strip -auto-orient %s", path));
	}

	/**
	*/
	void makeThumbnail(string srcPath, string outputPath, uint width, uint height)
	{
		auto command = format(
			"convert -strip -geometry %sx%s! %s %s",
			width,
			height,
			srcPath,
			outputPath
		);
		system(command);
	}
}

