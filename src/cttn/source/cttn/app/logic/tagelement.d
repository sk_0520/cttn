﻿/**
タグ。
*/
module cttn.app.logic.tagelement;

import std.array;

/**
*/
string encodeTag(string s)
{
	auto value = s;
	value = replace(value, "<", "&lt;");
	value = replace(value, ">", "&gt;");
	return value;
}

/**
えせDOM

定型作るだけならcreatre -> appendは個人的に超めんどいのさ。
テキスト直打ちもこれまた面倒なんでなんちゃって要素。
*/
class Element
{
	private string _tagName;
	protected Element[] _elements;
	protected RefAppender!(Element[]) _elementApp;
	protected bool _isXML;

	protected this()
	{
		this._elementApp = appender(&this._elements);
	}
	
	this(string tagName)
	in
	{
		assert(tagName.length);
	}
	body
	{
		this();
		this._tagName = tagName;
	}

	@property
	string tagName() const
	{
		return this._tagName;
	}

	protected void putElement(Element element)
	{
		this._elementApp.put(element);
	}

	abstract string toStringEx() const;
	override string toString() const
	{
		return toStringEx();
	}

	@property @safe
	{
		bool isXML() const
		{
			return this._isXML;
		}
		bool isXML(bool value)
		{
			return this._isXML = value;
		}
	}
}

/**
*/
class TextElement: Element
{
	string value;
	bool   encode;
	
	this(string value, bool encode)
	{
		super();
		this.value = value;
	}

	override string toStringEx() const
	{
		if(this.encode) {
			return encodeTag(this.value);
		} else {
			return this.value;
		}
	}
}

/**
*/
class TagElement: Element
{
	protected TagElement[]  _tags;
	protected TextElement[] _texts;
	protected RefAppender!(TagElement[])  _tagApp;
	protected RefAppender!(TextElement[]) _textApp;

	protected string[string] _attributes;
	
	this(string tagName)
	{
		super(tagName);
		this._tagApp  = appender(&this._tags);
		this._textApp = appender(&this._texts);
	}

	protected {
		void putTag(TagElement element)
		{
			this._tagApp.put(element);
		}
		void putText(TextElement element)
		{
			this._textApp.put(element);
		}
	}

	TagElement createTagElement(string tagName)
	{
		auto result = new TagElement(tagName);
		
		putTag(result);
		putElement(result);
		
		return result;
	}
	TextElement createTextElement(string value, bool encode = true)
	{
		auto result = new TextElement(value, encode);
		
		putText(result);
		putElement(result);
		
		return result;
	}

	bool hasAttribute(string attribute) const
	{
		return (attribute in this._attributes) !is null;
	}
	string getAttribute(string attribute) const
	{
		return this._attributes.get(attribute, null);
	}
	string setAttribute(string attribute, string value)
	{
		return this._attributes[attribute] = value;
	}
	string removeAttribute(string attribute)
	{
		if(auto p = attribute in this._attributes) {
			this._attributes.remove(attribute);
			return *p;
		}
		return null;
	}

	override string toStringEx() const
	{
		auto tag = appender!(string)();
		tag.put('<');
		tag.put(tagName);
		foreach(string key, const(string) value; this._attributes) {
			tag.put(' ');
			tag.put(key);
			tag.put('=');
			tag.put('"');
			tag.put(value);
			tag.put('"');
		}
		if(this._elements.length) {
			tag.put('>');
			//tag.put(std.string.newline);
			foreach(element; this._elements) {
				tag.put(element.toStringEx());
				//tag.put(std.string.newline);
			}
			tag.put("</");
			tag.put(tagName);
		} else if(this._isXML) {
			tag.put(" /");
		}
		tag.put('>');

		return tag.data;
	}
}

/**
*/
class RootElement: TagElement
{
	this(string tagName)
	{
		this._isXML = true;
		super(tagName);
	}
	alias super.isXML isXML;
	override bool isXML(bool value) @property @safe
	{
		foreach(element; this._elements) {
			element.isXML = value;
		}

		return value;
	}
	override {
		TagElement createTagElement(string tagName)
		{
			auto result = super.createTagElement(tagName);
			result.isXML = isXML;
			return result;
		}
		TextElement createTextElement(string value, bool encode)
		{
			auto result = super.createTextElement(value, encode);
			result.isXML = isXML;
			return result;
		}
	}
}

unittest {
	auto r=new RootElement("test");
	auto tag1=r.createTagElement("tag1");
	auto tag2=r.createTagElement("tag2");
	auto tag3=r.createTagElement("tag3");

	auto tag1_1=tag1.createTagElement("sub1");
	auto tag1_2=tag1.createTagElement("sub2");
	auto tag1_3=tag1.createTagElement("sub3");
	
	auto text1 = tag1.createTextElement("text text text...");
	auto text2 = tag2.createTextElement("TEXT TEXT TEXT...");
	auto tag1_4=tag1.createTagElement("sub4");
	auto tag1_4_1=tag1_4.createTagElement("sub_sub");

	r.setAttribute("lang", "ja-JP");
	tag1_3.setAttribute("attr", "bute");
	tag1_3.setAttribute("ATTR", "BUTE");
	assert(tag1_3.getAttribute("attr") == "bute");
	assert(tag1_3.getAttribute("ATTR") == "BUTE");
	tag1_3.removeAttribute("attr");

	//writeln(r);
}

