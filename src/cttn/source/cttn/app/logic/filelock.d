/**
filelock
*/
module cttn.app.logic.filelock;

import std.stream;
static import std.file;
static import std.path;
import std.exception;
import std.string;
import std.array;
import core.thread;
import std.conv;

import cttn.exception;

version(Windows) {
	import std.c.windows.windows;
	import std.windows.charset;
	alias _SECURITY_ATTRIBUTES SECURITY_ATTRIBUTES;
	alias _SECURITY_ATTRIBUTES* PSECURITY_ATTRIBUTES;
	alias _SECURITY_ATTRIBUTES* LPSECURITY_ATTRIBUTES;
	enum ERROR_ALREADY_EXISTS = 183;
	enum MUTANT_QUERY_STATE = 0x0001u;
	enum MUTANT_ALL_ACCESS = STANDARD_RIGHTS_REQUIRED|SYNCHRONIZE| MUTANT_QUERY_STATE;
	alias MUTANT_QUERY_STATE MUTEX_MODIFY_STATE;
	alias MUTANT_ALL_ACCESS MUTEX_ALL_ACCESS;
	struct _SECURITY_ATTRIBUTES {
		DWORD nLength;
		LPVOID lpSecurityDescriptor;
		BOOL bInheritHandle;
	}
	extern(Windows) export {
		BOOL ReleaseMutex(HANDLE _mutex);
		HANDLE CreateMutexA(
			LPSECURITY_ATTRIBUTES lpMutexAttributes,
			BOOL bInitialOwner,
			LPCSTR lpName
		);
		export HANDLE OpenMutexA(
			DWORD dwDesiredAccess,
			BOOL bInheritHandle,
			LPCSTR lpName
		);
	}
} else version(linux) {
	import std.c.linux.linux;
	import std.c.linux.pthread;
	import core.stdc.errno;
} else static assert(false);


/**
ファイルロック。

Params:
	TException = 例外。
*/
class TFileLock(TException: Exception)
{
	protected immutable string _path;
	protected const(char)* _name;
	version(Windows) {
		private HANDLE _mutex;
	} else {
		private int _mutex = -1;
	}

	/**
	Params:
		path = ロックファイル。

		retryMax = ロック再試行回数。

		waitTime = ロック待ち時間、ms。
	*/
	this(string path, size_t retryMax, size_t waitTime)
	in
	{
		assert(path.length > 1);
		assert(retryMax);
		assert(waitTime);
	}
	body
	{
		version(Windows) {
			string tempPath;
			if(path.length > "A:".length && path[1] == ':') {
				tempPath = path;
			} else {
				tempPath = std.path.buildNormalizedPath(std.file.getcwd(), path);
			}
			this._path = tempPath;
			this._name = this._path.replace("\\", "/").toMBSz();
		} else {
			if(path.length > "/".length && path[0] == '/') {
				this._path = path;
			} else {
				this._path = std.path.buildNormalizedPath(std.file.getcwd(), path);
			}
			auto tempPath = this._path.replace("/", ":");
			if(tempPath[0] == ':') {
				tempPath = '/' ~ tempPath[1 .. $];
			}
			this._name = tempPath.toStringz();
		}

		// ロック取得
		size_t counter;
		auto sleepTime = dur!("msecs")(waitTime);
		
		version(Windows) {
			auto mutexName = this._name;
			do {
				auto mutex = OpenMutexA(MUTEX_ALL_ACCESS, TRUE, mutexName);
				if(!mutex) {
					mutex = CreateMutexA(null, TRUE, mutexName);
					if(mutex && GetLastError() != ERROR_ALREADY_EXISTS) {
						this._mutex = mutex;
						break;
					} else {
						Thread.sleep(sleepTime);
						continue;
					}
				}
				auto waitResult = WaitForSingleObject(mutex, waitTime);
				if(waitResult == WAIT_OBJECT_0) {
					this._mutex = mutex;
					break;
				}
			} while(counter++ < retryMax);

			enforce(this._mutex, new TException(this._path));
		} else {
			auto mutexName = this._name;
			do {
				if(counter) {
					Thread.sleep(sleepTime);
				}
				auto fd = shm_open(mutexName, O_RDWR | O_CREAT, octal!600);
				if(fd != -1) {
					if(lockf(fd, F_TLOCK, 0) != -1) {
						this._mutex = fd;
						break;
					}
				}
			} while(counter++ < retryMax);
			enforce(this._mutex != -1, new TException(this._path));
		}
	}

	/**
	ロック解除。
	*/
	void release()
	{
		version(Windows) {
			if(this._mutex) {
				ReleaseMutex(this._mutex);
				CloseHandle(this._mutex);
				this._mutex = null;
			}
		} else {
			if(this._mutex != -1) {
				lockf(this._mutex, F_ULOCK, 0);
				shm_unlink(this._name);
			}
		}
	}

	~this()
	{
		release();
	}
}

TFileLock!(LockException) locker(string path, size_t retryMax, size_t waitTime)
{
	try {
		return new TFileLock!(LockException)(path, retryMax, waitTime);
	} catch(LockException ex) {
		return null;
	}
}

