/**
ファイルいろいろ。
*/
module cttn.app.logic.file;

import std.string;

/**
Bugs:
	境界チェック: 運用上問題なし。
*/
pure string toSizeString(ulong size)
{
	auto base = 1024;
	auto unit = ["byte", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
	auto value = size;
	auto i = 0u;
	
	while (value >= base)
	{
		value /= base;
		i++;
	}

	return format("%s%s", value, unit[i]);
}



