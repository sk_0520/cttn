/**
最初のページ。
*/
module cttn.app.controller.top_controller;

import vibe.d;

import cttn.config;
import cttn.mvc;
import cttn.app.process.top_process;
import cttn.app.model.top_model;


/**
*/
class TopController: CttnTargetController!(TopModel)
{
	override protected void doTargetProcess()
	{
		auto process = new TopInitProcess(this.common);
		process.execute(Parameter.init);
		render!("top", ViewMode.pc, ViewMode.touch);
	}
}
