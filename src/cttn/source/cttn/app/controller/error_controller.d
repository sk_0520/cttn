/**
エラーのページ。
*/
module cttn.app.controller.error_controller;

import vibe.d;

import cttn.config;
import cttn.mvc;
import cttn.app.model.error_model;
import cttn.app.process.log_process;


/**
*/
class ErrorController: CttnTargetController!(ErrorModel)
{
	override protected void doTargetProcess()
	{
		auto errorProcess = new LogErrorProcess(this.common);
		errorProcess.execute(Parameter.init);
		
		render!("error", ViewMode.pc);
	}
}
