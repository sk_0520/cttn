/**
ブログ編集。
*/
module cttn.app.process.blog_edit_process;

import std.algorithm;
import std.regex;

import vibe.d;

import cttn.config;
import cttn.utility;
import cttn.mvc;
import cttn.app.model.blog_edit_model;
import cttn.app.process.blog_base_process;
import cttn.app.storage.blog_storage;


abstract class BlogEditBaseProcess: BlogBaseProcess!(BlogEditModel)
{
	mixin CttnTargetProcessBuilder!(BlogEditModel);

	protected override void enforceProcess()
	{
		super.enforceProcess();

		debug { } else {
			enforceHTTP(this.common.login.isLogin, HTTPStatus.forbidden);
		}
	}

}

class BlogEditEditProcess: BlogEditBaseProcess
{
	mixin CttnTargetProcessBuilder!(BlogEditModel);

	override ExecuteResult executeTarget(Parameter exParam)
	{
		auto storage = new BlogStorage(this.common.db.data);

		this.common.model.allTags = getAllTagList(storage);

		if(this.common.model.editMode == BlogEditMode.create) {
			// 新規作成
		} else {
			// 編集
			auto content = storage.getContent(this.common.model.targetId, true);
			auto tags = storage.getTagListFromId(this.common.model.targetId);
			
			this.common.model.targetTitle   = content.title;
			this.common.model.targetContent = content.content;
			this.common.model.targetTags    = tags.map!(t => t.name).array();
			this.common.model.createTimestamp = content.createTimestamp;
		}
		
		return ExecuteResult.success;
	}
}

class BlogEditPostProcess: BlogEditBaseProcess
{
	mixin CttnTargetProcessBuilder!(BlogEditModel);

	protected override void enforceProcess()
	{
		super.enforceProcess();

		enforceHTTP(this.common.req.method == HTTPMethod.POST, HTTPStatus.badRequest);
	}
	
	ExecuteResult check()
	{
		auto result = new ExecuteResult(true);

		// タイトルチェック
		auto unsafeTitle = this.common.model.targetTitle.strip();
		if(!unsafeTitle.length) {
			this.common.model.errorTargetTitle = true;
			result.preservationTrue = false;
		}
		
		// 本文チェック
		auto unsafeContent = this.common.model.targetContent.strip();
		if(!unsafeContent.length) {
			this.common.model.errorTargetContent = true;
			result.preservationTrue = false;
		}
		
		// タグチェック
		auto reg = ctRegex!(`([\s,\.\[\]\{\}\*;:\/\\])`);
		auto errorTags = this.common.model.targetTags.filter!(s => match(s, reg)).array();
		if(errorTags.length) {
			this.common.model.errorTargetTags = errorTags;
			result.preservationTrue = false;
		}
		
		return result;
	}

	void correction()
	{
		this.common.model.targetTitle   = this.common.model.targetTitle.strip();
		this.common.model.targetContent = this.common.model.targetContent.stripRight();
	}
	
	override ExecuteResult executeTarget(Parameter exParam)
	{
		auto result = check();
		
		auto storage = new BlogStorage(this.common.db.data);
		this.common.model.allTags = getAllTagList(storage);
		
		
		if(result.eval) {
			storage.beginTransaction();
			scope(success) storage.commit();
			scope(failure) storage.rollback();
			
			// 使用ID取得。
			auto useId = this.common.model.targetId;
			bool isCreate = !useId;
			if(isCreate) {
				auto maxId = storage.getNumber("BLOG_HEAD", "BLOG_ID", GetNumber.max);
				useId = cast(BlogId)(maxId + 1);
			}
			
			correction();
			// 書き込み
			storage.registBlogContent(useId, this.common.model.targetTitle, this.common.model.targetContent, this.common.model.targetTags);
		} else if(this.common.model.targetId) {
			auto content = storage.getContent(this.common.model.targetId, true);
			this.common.model.createTimestamp = content.createTimestamp;
		}

		return result;
	}
}
class BlogEditListProcess: BlogEditBaseProcess
{
	mixin CttnTargetProcessBuilder!(BlogEditModel);
	
	override ExecuteResult executeTarget(Parameter exParam)
	{
		auto storage = new BlogStorage(this.common.db.data);

		this.common.model.yearMonth = getYearMonth(storage, true);
		
		if(!this.common.model.isSafeYear) {
			this.common.model.year = this.common.model.startTime.year;
		}
		if(!this.common.model.isSafeMonth) {
			this.common.model.month = 0;
		}
		this.common.model.titleList    = storage.getTitleList(this.common.model.year, this.common.model.month, this.common.model.orderByAsc, true);
		this.common.model.contentsTags = getContentTags(storage, this.common.model.titleList.map!(t => t.id).array());
		
		return ExecuteResult.success;
	}
}

class BlogEditSwitchProcess: BlogEditBaseProcess
{
	mixin CttnTargetProcessBuilder!(BlogEditModel);
	
	override ExecuteResult executeTarget(Parameter exParam)
	{
		auto result = new ExecuteResult(true);
		if(this.common.model.switchTargets.length) {
			auto storage = new BlogStorage(this.common.db.data);
			storage.beginTransaction();
			scope(success) storage.commit();
			scope(failure) storage.rollback();
			
			// 有効・削除反転！
			storage.switchId(this.common.model.switchTargets);
			result.preservationTrue = true;
		} else {
			this.common.model.errorSwitchTargetsNoCheck = true;
			result.preservationTrue = false;
		}
		
		
		return result;
	}
}

