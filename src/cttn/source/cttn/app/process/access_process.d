/**
アクセス情報。
*/
module cttn.app.process.access_process;

import vibe.d;

import cttn.config;
import cttn.mvc;
import cttn.app.model.access_model;
import cttn.app.storage.log_storage;


/**
*/
abstract class AccessProcess: CttnTargetProcess!(AccessModel)
{
	mixin CttnTargetProcessBuilder!(AccessModel);

	protected override void enforceProcess()
	{
		super.enforceProcess();

		debug { } else {
			enforceHTTP(this.common.login.isLogin, HTTPStatus.forbidden);
		}
	}
}


/**
*/
class AccessInitProcess: AccessProcess
{
	mixin CttnTargetProcessBuilder!(AccessModel);

	override ExecuteResult executeTarget(Parameter exParam)
	{
		auto strage = new LogStorage(this.common.db.log);
		
		this.common.model.pageCount        = Count(strage.getPageCount());
		this.common.model.errorCount       = Count(strage.getErrorCount());
		
		this.common.model.browserTotalUA   = Count(strage.getBrowserTotalUA());
		this.common.model.browserVersionUA = Count(strage.getBrowserVersionUA());
		
		this.common.model.osTotalUA   = Count(strage.getOsTotalUA());
		this.common.model.osVersionUA = Count(strage.getOsVersionUA());
		
		this.common.model.referer          = Count(strage.getReferer(this.common.conf.hostName));
		return ExecuteResult.success;
	}
}

