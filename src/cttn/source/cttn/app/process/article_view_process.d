/**
記事表示。
*/
module cttn.app.process.article_view_process;

import vibe.d;

import cttn.config;
import cttn.mvc;
import cttn.app.model.article_base_model;
import cttn.app.model.article_view_model;
import cttn.app.process.article_base_process;
import cttn.app.storage.article_storage;
import cttn.app.storage.file_storage;
import cttn.app.logic.markdown;


abstract class ArticleViewBaseProcess: ArticleBaseProcess!(ArticleViewModel)
{
	mixin CttnTargetProcessBuilder!(ArticleViewModel);
}


class ArticleViewInitProcess: ArticleBaseProcess!(ArticleViewModel), IInitializeProcess
{
	mixin CttnTargetProcessBuilder!(ArticleViewModel);
	mixin InitializeProcessBuilder;

	override ExecuteResult executeTarget(Parameter exParam)
	{
		auto storage = new ArticleStorage(this.common.db.data);

		getArticleList(storage, this.common.login.isLogin);
		
		return ExecuteResult.success;
	}
}


class ArticleViewListProcess: ArticleBaseProcess!(ArticleViewModel)
{
	mixin CttnTargetProcessBuilder!(ArticleViewModel);

	override ExecuteResult executeTarget(Parameter exParam)
	{
		auto storage = new ArticleStorage(this.common.db.data);
		
		this.common.model.viewGroup = getGroup(storage, this.common.model.targetGroupId, this.common.login.isLogin);
		this.common.model.viewTitleList = getTitleList(storage, this.common.model.targetGroupId, this.common.login.isLogin);
		
		return ExecuteResult.success;
	}
}

class ArticleViewPageProcess: ArticleBaseProcess!(ArticleViewModel)
{
	mixin CttnTargetProcessBuilder!(ArticleViewModel);

	override ExecuteResult executeTarget(Parameter exParam)
	{
		auto storage = new ArticleStorage(this.common.db.data);

		ArticlePage page;
		page.dto = storage.getPage(this.common.model.targetId, this.common.login.isLogin);
		page.markdownContent = replaceMarkdown(page.dto.content, true, this.common.login.isLogin, new FileStorage(this.common.db.data));
		this.common.model.viewPage = page;
		this.common.model.viewGroup = getGroup(storage, this.common.model.viewPage.groupId, true);
		
		return ExecuteResult.success;
	}
}


