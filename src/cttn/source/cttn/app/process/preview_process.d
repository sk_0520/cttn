/**
最初のページ。
*/
module cttn.app.process.preview_process;

import vibe.d;

import cttn.mvc;
import cttn.app.logic.markdown;
import cttn.app.storage.file_storage;
import cttn.app.model.preview_model;


abstract class PreviewBaseProcess: CttnTargetProcess!(PreviewModel)
{
	mixin CttnTargetProcessBuilder!(PreviewModel);
}


class PreviewInitProcess: PreviewBaseProcess
{
	mixin CttnTargetProcessBuilder!(PreviewModel);

	protected override void enforceProcess()
	{
		super.enforceProcess();

		enforceHTTP(this.common.req.method == HTTPMethod.POST, HTTPStatus.badRequest);
	}
	
	override ExecuteResult executeTarget(Parameter exParam)
	{
		this.common.model.contentMarkdown = replaceMarkdown(this.common.model.contentBody, this.common.model.inDownload, this.common.login.isLogin, new FileStorage(this.common.db.data));
		
		return ExecuteResult.success;
	}
}

