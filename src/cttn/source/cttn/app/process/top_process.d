/**
最初のページ。
*/
module cttn.app.process.top_process;

import vibe.d;

import cttn.config;
import cttn.mvc;
import cttn.app.model.top_model;


class TopInitProcess: CttnTargetProcess!(TopModel)
{
	mixin CttnTargetProcessBuilder!(TopModel);

	override ExecuteResult executeTarget(Parameter exParam)
	{
		return ExecuteResult.success;
	}
}

