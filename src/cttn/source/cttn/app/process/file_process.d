/**
ファイル。
*/
module cttn.app.process.file_process;

import std.algorithm;
import std.array;
import std.conv;
import std.string;
import std.stream;
import std.file;
import std.process;
import std.range;

import vibe.d;

import cttn.literal;
import cttn.config;
import cttn.type;
import cttn.utility;
import cttn.mvc;
import cttn.app.storage.file_storage;
import cttn.app.entity.file_entity;
import cttn.app.model.file_model;
import cttn.app.logic.image;

class FileUploadParameter: Parameter
{
	string parentDirPath;

	this(string parentDirPath)
	{
		this.parentDirPath = parentDirPath;
	}
}

abstract class FileBaseProcess: CttnTargetProcess!(FileModel, FileUploadParameter)
{
	mixin CttnTargetProcessBuilder!(FileModel);

	void check(size_t fileLength, UploadFile[] fileList) const { }
	
	struct UploadFile
	{
		string reqName;
		string title;
		string path;
		string name;
	}

	/**
	Bugs:
		ファイル名重複, とりあえず運用回避
	*/
	UploadFile[] getUploadFile() const
	{
		size_t fileLength;
		convertFromString(this.rawCommon.req.form["upload_length"], fileLength);

		
		auto app = appender!(UploadFile[])();

		foreach(i; 0 .. fileLength) {
			auto titleKey = format("upload_title_%s", i + 1);
			auto nameKey  = format("upload_name_%s", i + 1);
			auto fileKey  = format("upload_file_%s", i + 1);
			
			UploadFile file;
			if(fileKey !in this.rawCommon.req.files) {
				continue;
			}
			auto filePart = this.rawCommon.req.files[fileKey];
			file.reqName = filePart.filename.toString();
			if(nameKey in this.rawCommon.req.form) {
				convertFromString(this.rawCommon.req.form[nameKey],  file.name);
			}
			if(!file.name.length) {
				file.name = file.reqName;
			}
			if(titleKey in this.rawCommon.req.form) {
				convertFromString(this.rawCommon.req.form[titleKey], file.title);
			}
			if(!file.title.length) {
				file.title = file.reqName;
			}
			file.path    = filePart.tempPath.toNativeString();
			app.put(file);
		}

		auto fileList = app.data();
		
		check(fileLength, fileList);

		return fileList;
	}

	void moveFile(string[] fromPath, string[] toPath, void delegate(string[] to) movedDg = null) const
	in
	{
		assert(fromPath.length == toPath.length);
	}
	body
	{
		auto removeList = appender!(string[])();

		foreach(z; zip(fromPath, toPath)) {
			auto f = z[0];
			auto t = z[1];
			try {
				rename(f, t);
			} catch(FileException ex) {
				copy(f, t);
				removeList.put(f);
			}
		}

		system("sync");
		foreach(t; toPath) {
			if(isImageExt(t)) {
				// Exif情報削除
				
				ImageMagic.stripExif(t);
				system("sync");
				// サムネイル作成
				auto thumbnailPath = toThumbnailPath(t, Literal.thumbnailSuffix);
				ImageMagic.makeThumbnail(t, thumbnailPath, this.common.conf.thumbnailWidth, this.common.conf.thumbnailHeight);
			}
		}
		
		if(movedDg) {
			movedDg(toPath);
		}
		
		foreach(path; removeList.data()) {
			remove(path);
		}
	}
}



class FilePostProcess: FileBaseProcess
{
	mixin CttnTargetProcessBuilder!(FileModel);

	override protected void enforceProcess()
	{
		super.enforceProcess();
		
		debug { } else {
			enforceHTTP(this.common.login.isLogin, HTTPStatus.forbidden);
		}
		enforceHTTP(this.common.req.method == HTTPMethod.POST, HTTPStatus.badRequest);
	}
	
	override ExecuteResult executeTarget(FileUploadParameter exParam)
	{
		// TODO: いつかやる
		/+
		auto storage = new FileStorage(this.common.db.data);
		storage.beginTransaction();
		scope(success) storage.commit();
		scope(failure) storage.rollback();
		+/
	
		return ExecuteResult.failure;
	}
}

class FileInitProcess: FileBaseProcess, IInitializeProcess
{
	mixin CttnTargetProcessBuilder!(FileModel);
	mixin InitializeProcessBuilder;

	override protected void enforceProcess()
	{
		super.enforceProcess();
		
		debug { } else {
			enforceHTTP(this.common.login.isLogin, HTTPStatus.forbidden);
		}
	}
	
	override ExecuteResult executeTarget(FileUploadParameter exParam)
	{
		auto storage = new FileStorage(this.common.db.data);
		
		string[] keys;
		if(this.common.model.targetKey.length) {
			keys = [this.common.model.targetKey];
		} else {
			keys = map!(d => d.key)(storage.getFileKeys(true)).array();
		}
		foreach(key; keys) {
			auto list = storage.getFileInfo(key, true);
			this.common.model.fileInfoList[key] = list;
		}
		
		return ExecuteResult.success;
	}
}


class FileUploadProcess: FileBaseProcess
{
	mixin CttnTargetProcessBuilder!(FileModel);

	public enum uploadSubDirKey = "targetKey";

	override protected void enforceProcess()
	{
		super.enforceProcess();
		
		debug { } else {
			enforceHTTP(this.common.login.isLogin, HTTPStatus.forbidden);
		}
		enforceHTTP(this.common.req.method == HTTPMethod.POST, HTTPStatus.badRequest);
	}

	/**
	*/
	override ExecuteResult executeTarget(FileUploadParameter exParam)
	in
	{
		assert(exParam);
	}
	body
	{
		auto fileList = getUploadFile();

		// データ保存
		auto time = getNowTime();
		// 別モジュールに切り出すかもしれないので極力モデルを使用しない
		string subDirName;
		if(uploadSubDirKey in this.rawCommon.req.form) {
			subDirName = this.rawCommon.req.form[uploadSubDirKey].strip();
		}
		if(!subDirName.length) {
			subDirName = "upload";
		}
		
		auto parentDirPath = buildPath(exParam.parentDirPath, subDirName, time.toFileString());
		makeDir(parentDirPath);
		moveFile(fileList.map!(f => f.path).array(), fileList.map!(f => buildPath(parentDirPath, f.name)).array(), delegate(to) {
			auto storage = new FileStorage(this.common.db.data);
			storage.beginTransaction();
			scope(success) storage.commit();
			scope(failure) storage.rollback();
			
			foreach(i; 0 .. fileList.length) {
				auto f = fileList[i];
				ulong size;
				if(exists(to[i])) {
					size = getSize(to[i]);
				}
				storage.registFilePath(FileId.init, subDirName, f.title, to[i], f.name, size);
			}
		});

		return ExecuteResult.success;
	}
}


class FileDownloadProcess: FileBaseProcess
{
	mixin CttnTargetProcessBuilder!(FileModel);
	
	override ExecuteResult executeTarget(FileUploadParameter exParam)
	{
		auto storage = new FileStorage(this.common.db.data);
		auto result = storage.getDownload(this.common.model.targetId, this.common.login.isLogin);
		enforceHTTP(result.enable, HTTPStatus.notFound);
		
		string disposition;
		if(this.common.model.save) {
			disposition = "attachment";
		} else {
			disposition = "inline";
		}
		auto path = result.path;
		auto size = result.size;
		if(this.common.model.thumbnail && isImageExt(path)) {
			auto thumbnailPath = toThumbnailPath(path, Literal.thumbnailSuffix);
			if(exists(thumbnailPath)) {
				path = thumbnailPath;
				size = getSize(path);
			}
		}
		enforceHTTP(exists(path), HTTPStatus.notFound);
		
		common.res.headers["Content-Disposition"] = format(`%s; filename="%s"`, disposition, result.name);
		common.res.headers["Content-Length"]      = to!(string)(size);


		auto inputStream = new BufferedFile(path);
		scope(exit) inputStream.close();
		auto outputStream = this.common.res.bodyWriter;
		auto buffer = new ubyte[4 * 1024];
		while(!inputStream.eof) {
			auto length = inputStream.read(buffer);
			outputStream.write(buffer[0 .. length]);
		}

		return ExecuteResult.success;
	}
}


