/**
管理者。
*/
module cttn.app.process.admin_process;

import std.digest.sha;

import vibe.d;

import cttn.literal;
import cttn.config;
import cttn.mvc;
import cttn.utility;
import cttn.service;
import cttn.app.storage.cache_storage;
import cttn.app.model.admin_model;

class AdminInitProcess: CttnTargetProcess!(AdminModel)
{
	mixin CttnTargetProcessBuilder!(AdminModel);
	
	override ExecuteResult executeTarget(Parameter exParam)
	{
		setHeaderNoCache();
		
		return ExecuteResult.success;
	}
}

class AdminLoginProcess: CttnTargetProcess!(AdminModel)
{
	mixin CttnTargetProcessBuilder!(AdminModel);

	protected override void enforceProcess()
	{
		super.enforceProcess();
		
		enforceHTTP(this.common.req.method == HTTPMethod.POST, HTTPStatus.badRequest);
	}
	
	private bool authLogin(string loginName, string loginPass) const
	{
		if(loginName == this.common.conf.loginName) {
			auto sha = new SHA1Digest();
			if(toHexString(sha.digest(loginPass)) == this.common.conf.loginPass) {
				return true;
			}
		}

		return false;
	}
	
	override ExecuteResult executeTarget(Parameter exParam)
	{
		auto checkAuth = authLogin(this.common.model.loginName, this.common.model.loginPass);

		setHeaderNoCache();
		
		auto login = this.common.login;
		this.common.model.state = LoginState.failure;
		if(checkAuth) {
			if(!login.isLogin) {
				// セッションの登録
				auto tran = this.common.db.cache.beginTransaction();
				scope(success) {
					tran.commit();
					this.common.model.state = LoginState.success;
				}
				scope(failure) {
					tran.rollback();
					this.common.model.state = LoginState.failure;
				}
				
				auto nowTime = getNowTime();
				auto sha = new SHA1Digest();
				login.key  = toHexString(sha.digest(nowTime.toString()));
				login.name = this.common.model.loginName;
				login.lastAccess = nowTime;
				login.isLogin = true;
				//setLogin(this.common.conf.pathSessionFile, session, login, this.common.db.cache);
				//setLogin(session, this.common.db.cache, login);
				auto storage = new LoginSessionStorage(this.common.db.cache);
				storage.deleteLoginSession(login.name);
				storage.insertLoginSession(login);
				
				auto sessionStorage = new SessionStorage(this.common.res.startSession());
				sessionStorage.setKey(Literal.sessionKey, login.key);
			}
			this.common.login = login;
		} else {
			if(login.isLogin) {
				//this.common.model.message = "failure: session logout";
				this.common.res.terminateSession();
				this.common.model.state = LoginState.none;
			//} else {
			//	this.common.model.message = "failure";
			}
			login.isLogin = false;
		}
		
		return new ExecuteResult(checkAuth);
	}
}

version(none)
unittest
{
	auto sha = new SHA1Digest();
	std.stdio.writeln(toHexString(sha.digest("")));
}

class AdminLogoutProcess: CttnTargetProcess!(AdminModel)
{
	mixin CttnTargetProcessBuilder!(AdminModel);
	
	override ExecuteResult executeTarget(Parameter exParam)
	{
		setHeaderNoCache();
		
		if(this.common.req.session) {
			this.common.res.terminateSession();
		}
		
		return ExecuteResult.success;
	}
}
