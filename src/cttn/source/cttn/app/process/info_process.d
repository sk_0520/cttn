/**
情報表示。
*/
module cttn.app.process.info_process;

import vibe.d;

import cttn.config;
import cttn.mvc;
import cttn.app.model.info_model;


class InfoInitProcess: CttnTargetProcess!(InfoModel)
{
	mixin CttnTargetProcessBuilder!(InfoModel);

	override ExecuteResult executeTarget(Parameter exParam)
	{
		// TODO: 外部化
		this.common.model.libraryList = [
			Library("vibe.d",            "http://vibed.org/"),
			Library("d2sqlite3",         "https://github.com/biozic/d2sqlite3"),
		
			Library("jQuery UI",         "http://jqueryui.com/"),
			Library("PageDown",          "http://code.google.com/p/pagedown/wiki/PageDown"),
			Library("Uniform",           "http://uniformjs.com/"),
			Library("SyntaxHighlighter", "http://alexgorbatchev.com/SyntaxHighlighter/"),
			Library("tabIndent.js",      "http://julianlam.github.com/tabIndent.js/"),
			Library("Autosize",          "http://www.jacklmoore.com"),
		
			Library("normalize.css",     "http://necolas.github.io/normalize.css/"),
		
			Library("ImageMagick",       "http://www.imagemagick.org/"),
		
			Library("Aleksandra Wolska", "http://olawolska.com/"),
		];
		return ExecuteResult.success;
	}
}

