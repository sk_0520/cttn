/**
記事編集。
*/
module cttn.app.process.article_edit_process;

import vibe.d;

import cttn.config;
import cttn.mvc;
import cttn.utility;
import cttn.app.model.article_base_model;
import cttn.app.model.article_edit_model;
import cttn.app.process.article_base_process;
import cttn.app.storage.article_storage;


abstract class ArticleEditBaseProcess: ArticleBaseProcess!(ArticleEditModel)
{
	mixin CttnTargetProcessBuilder!(ArticleEditModel);
	
	protected override void enforceProcess()
	{
		super.enforceProcess();

		debug { } else {
			enforceHTTP(this.common.login.isLogin, HTTPStatus.forbidden);
		}
	}

	bool enableGroupName(ArticleStorage storage, ArticleGroupId groupId, string groupName)
	in
	{
		assert(groupName == groupName.strip());
	}
	body
	{
		if(!groupName.length) {
			return false;
		}

		if(storage.hasGroupName(groupId, groupName)) {
			return false;
		}

		return true;
	}

	bool enablepageTitle(ArticleStorage storage, ArticleGroupId groupId, ArticleId id, string title)
	in
	{
		assert(title == title.strip());
	}
	body
	{
		if(!title.length) {
			return false;
		}

		if(storage.hasPageTitle(groupId, id, title)) {
			return false;
		}

		return true;
	}
}


class ArticleEditInitProcess: ArticleEditBaseProcess, IInitializeProcess
{
	mixin CttnTargetProcessBuilder!(ArticleEditModel);
	mixin InitializeProcessBuilder;
	
	override ExecuteResult executeTarget(Parameter exParam)
	{
		auto storage = new ArticleStorage(this.common.db.data);

		getArticleList(storage, true);
		
		return ExecuteResult.success;
	}
}


class ArticleEditPostProcess: ArticleEditBaseProcess
{
	mixin CttnTargetProcessBuilder!(ArticleEditModel);

	protected override void enforceProcess()
	{
		super.enforceProcess();

		enforceHTTP(this.common.req.method == HTTPMethod.POST, HTTPStatus.badRequest);
	}
	
	private ExecuteResult postGroupEdit(ArticleStorage storage)
	{
		auto groupName = this.common.model.targetGroupName.strip();
		if(!enableGroupName(storage, this.common.model.targetGroupId, groupName)) {
			this.common.model.errorTargetGroupName = true;
			return ExecuteResult.failure;
		}
		
		storage.beginTransaction();
		scope(success) storage.commit();
		scope(failure) storage.rollback();
		storage.registGroup(this.common.model.targetGroupId, groupName);
		this.common.model.targetGroupName = groupName;

		return ExecuteResult.success;
	}
	
	private ExecuteResult postPageEdit(ArticleStorage storage)
	{
		auto title = this.common.model.targetTitle.strip();
		if(!enablepageTitle(storage, this.common.model.targetGroupId, this.common.model.targetId, title)) {
			this.common.model.errorTargetTitle = true;
			return ExecuteResult.failure;
		}

		// 本文チェック
		auto unsafeContent = this.common.model.targetContent.strip();
		if(!unsafeContent.length) {
			this.common.model.errorTargetContent = true;
			return ExecuteResult.failure;
		}

		storage.beginTransaction();
		scope(success) storage.commit();
		scope(failure) storage.rollback();
		storage.registArticleContent(this.common.model.targetGroupId, this.common.model.targetId, this.common.model.targetTitle, this.common.model.targetContent);
		
		return ExecuteResult.success;
	}
	private ExecuteResult postPageSwitch(ArticleStorage storage)
	{
		auto result = new ExecuteResult(true);
		if(this.common.model.switchPageTargets.length) {
			storage.beginTransaction();
			scope(success) storage.commit();
			scope(failure) storage.rollback();
			
			// 有効・削除反転！
			storage.switchPageId(this.common.model.switchPageTargets);
			result.preservationTrue = true;
		} else {
			this.common.model.errorSwitchPageTargetsNoCheck = true;
			result.preservationTrue = false;
		}
		
		
		return result;
	}

	override ExecuteResult executeTarget(Parameter exParam)
	{
		auto storage = new ArticleStorage(this.common.db.data);
		switch(this.common.model.mode) {
			case ArticleMode.groupEdit:
				return postGroupEdit(storage);
			case ArticleMode.pageEdit:
				return postPageEdit(storage);
			case ArticleMode.pageSwitch:
				return postPageSwitch(storage);
			default:
				return ExecuteResult.failure;
		}
	}
}

class ArticleEditEditProcess: ArticleEditBaseProcess
{
	mixin CttnTargetProcessBuilder!(ArticleEditModel);
	
	override ExecuteResult executeTarget(Parameter exParam)
	{
		if(this.common.model.targetId) {
			// 既存読み込み
			auto storage = new ArticleStorage(this.common.db.data);
			auto dto = storage.getPage(this.common.model.targetId, true);
			this.common.model.targetTitle   = dto.title;
			this.common.model.targetContent = dto.content;
			this.common.model.createTimestamp = dto.createTimestamp;
		}
		
		return ExecuteResult.success;
	}
}

