/**
ログ処理。
*/
module cttn.app.process.log_process;

import vibe.d;

import cttn.config;
import cttn.mvc;
import cttn.app.storage.log_storage;
import cttn.app.model.error_model;


class LogAccessProcess: CttnProcess
{
	mixin CttnProcessBuilder;

	override ExecuteResult execute(Parameter exRawParam)
	{
		auto storage = new LogStorage(this.rawCommon.db.log);
		storage.beginTransaction();
		scope(success) storage.commit();
		scope(failure) storage.rollback();
		
		storage.registerAccess(this.rawCommon.req);
		
		return ExecuteResult.success;
	}
}



class LogErrorProcess: CttnTargetProcess!(ErrorModel)
{
	mixin CttnTargetProcessBuilder!(ErrorModel);

	override ExecuteResult executeTarget(Parameter exParam)
	{
		/+
		auto storage = new LogStorage(this.rawCommon.db.log);
		storage.beginTransaction();
		scope(success) storage.commit();
		scope(failure) storage.rollback();
		
		storage.registerError(this.common.req, this.common.model.error);
		+/
		return ExecuteResult.success;
	}
}

