/**
記事。
*/
module cttn.app.process.article_base_process;

import std.algorithm;

import vibe.d;

import cttn.config;
import cttn.mvc;
import cttn.app.dto.article_dto;
import cttn.app.model.article_base_model;
import cttn.app.storage.article_storage;


abstract class ArticleBaseProcess(TModel: ArticleBaseModel): CttnTargetProcess!(TModel)
{
	mixin CttnTargetProcessBuilder!(TModel);

	ArticleGroupDto getGroup(ArticleStorage storage, ArticleGroupId groupId, bool scanDelete)
	{
		return storage.getGroup(groupId, scanDelete);
	}

	ArticleTitleDto[] getTitleList(ArticleStorage storage, ArticleGroupId groupId, bool scanDelete)
	{
		return storage.getTitleList(groupId, scanDelete);
	}

	void getArticleList(ArticleStorage storage, bool scanDelete)
	{
		this.common.model.groupList =  storage.getGroupList(scanDelete);
		foreach(groupDto; this.common.model.groupList) {
			auto groupId = groupDto.groupId;
			auto titleList = getTitleList(storage, groupId, scanDelete);
			this.common.model.titleList[groupId] = titleList;
		}
	}

	override ExecuteResult executeTarget(Parameter exParam)
	{
		assert(false);
	}
}

