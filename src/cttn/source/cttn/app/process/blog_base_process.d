/**
ブログ。
*/
module cttn.app.process.blog_base_process;

import std.algorithm;
import std.array;

import vibe.d;

import cttn.config;
import cttn.mvc;
import cttn.app.model.blog_base_model;
import cttn.app.dto.blog_dto;
import cttn.app.storage.blog_storage;
import cttn.app.storage.file_storage;
import cttn.app.logic.markdown;


abstract class BlogBaseProcess(TModel: BlogBaseModel): CttnTargetProcess!(TModel)
{
	mixin CttnTargetProcessBuilder!(TModel);

	protected string[] getAllTagList(BlogStorage storage)
	in
	{
		assert(storage);
	}
	body
	{
		auto allTags = storage.getTagList();
		return allTags.map!(t => t.name).array();
	}
	
	protected typeof(BlogBaseModel.yearMonth) getYearMonth(BlogStorage storage, bool scanDelete)
	in
	{
		assert(storage);
	}
	body
	{
		typeof(BlogBaseModel.yearMonth) result;
		
		auto yearMonthList = storage.getYearMonthList(scanDelete);
		foreach(ym; yearMonthList) {
			result[ym.year] ~= ym.month;
		}
		foreach(key; result.keys) {
			result[key].sort;
		}
		
		return result;
	}

	protected typeof(BlogBaseModel.contentsTags) getContentTags(BlogStorage storage, BlogId[] idList)
	{
		typeof(BlogBaseModel.contentsTags) result;

		if(idList.length) {
			auto tagList = storage.getTagListFromIdList(idList);
			foreach(value; tagList) {
				result[value.id] ~= value.name;
			}
		}
		
		return result;
	}

	BlogContent toBlogContent(BlogContentDto dto, bool scanDelete, FileStorage storage)
	{
		BlogContent result;
		result.dto = dto;
		result.markdownContent = replaceMarkdown(result.dto.content, true, scanDelete, storage);
		return result;
	}
	BlogContent[] toBlogContentList(BlogContentDto[] dtoList, bool scanDelete, FileStorage storage)
	{
		return dtoList.map!(dto => toBlogContent(dto, scanDelete, storage))().array();
	}
	

	override ExecuteResult executeTarget(Parameter exParam)
	{
		assert(false);
	}
}

