/**
ブログ表示。
*/
module cttn.app.process.blog_view_process;

import std.algorithm;
import std.array;
import std.ascii;
import std.variant;

import vibe.d;

import cttn.config;
import cttn.utility;
import cttn.mvc;
import cttn.exception;
import cttn.app.model.blog_view_model;
import cttn.app.process.blog_base_process;
import cttn.app.storage.blog_storage;
import cttn.app.storage.file_storage;
import cttn.app.logic.markdown;


abstract class BlogViewBaseProcess: BlogBaseProcess!(BlogViewModel)
{
	mixin CttnTargetProcessBuilder!(BlogViewModel);

}

class BlogViewInitProcess: BlogViewBaseProcess, IInitializeProcess
{
	mixin CttnTargetProcessBuilder!(BlogViewModel);
	mixin InitializeProcessBuilder;

	void setPageMove(ExDBStorage storage, size_t offset, size_t limit, bool scanDelete, string tableName, string whereAppend, Variant[string] params)
	{
		auto whereCommand = scanDelete ? "": "CMN_DELETE is null";
		if(whereAppend.length) {
			whereCommand ~= newline ~ whereAppend;
		}
		auto rowCount = storage.getNumber(tableName, "*", GetNumber.count, whereCommand, params);
		this.common.model.future = offset >= limit && (offset - limit) <= limit;
		this.common.model.old    = (offset + limit) < rowCount;
	}

	override ExecuteResult executeTarget(Parameter exParam)
	{
		auto storage = new BlogStorage(this.common.db.data);
		auto fileStorage = new FileStorage(this.common.db.data);
		this.common.model.allTags = getAllTagList(storage);
		this.common.model.yearMonth = getYearMonth(storage, false);
		string countWhereCommand;
		string tableName;
		Variant[string] params;
		if(this.common.model.viewType == ViewType.archive) {
			//this.common.model.contentList = storage.getContentArchive(this.common.model.year, this.common.model.month, this.common.model.orderByAsc, false);
			this.common.model.contentList = toBlogContentList(storage.getContentArchive(this.common.model.year, this.common.model.month, this.common.model.orderByAsc, false), this.common.login.isLogin, fileStorage);
		} else {
			if(this.common.model.viewType == ViewType.tag && this.common.model.targetTag.length) {
				this.common.model.contentList = toBlogContentList(storage.getContentNormalListFromTag(this.common.model.targetTag, this.common.model.offsetPage, this.common.model.limitPage, this.common.model.orderByAsc, false), this.common.login.isLogin, fileStorage);
				tableName = "BLOG_HEAD inner join BLOG_TAG BT on BLOG_HEAD.BLOG_ID = BT.BLOG_ID";
				countWhereCommand = "and BT.BLOG_TAG_NAME = :tagName";
				params["tagName"] = this.common.model.targetTag;
			} else {
				bool isSingle = this.common.model.viewType == ViewType.single && this.common.model.targetId != BlogId.init;
				if(isSingle) {
					try {
						auto dto = storage.getContent(this.common.model.targetId, false);
						this.common.model.contentList = toBlogContentList([dto], this.common.login.isLogin, fileStorage);
					} catch(CttnException ex) {
						cttn.utility.print(ex.toString());
						isSingle = false;
					}
				}
				if(!isSingle) {
					this.common.model.contentList = toBlogContentList(storage.getContentNormalList(this.common.model.offsetPage, this.common.model.limitPage, this.common.model.orderByAsc, false), this.common.login.isLogin, fileStorage);
				}
				tableName = "BLOG_HEAD";
			}
			setPageMove(storage, this.common.model.offsetPage, this.common.model.limitPage, false, tableName, countWhereCommand, params);
		}
		this.common.model.contentsTags = getContentTags(storage, this.common.model.contentList.map!(t => t.dto.id).array());
		
		return ExecuteResult.success;
	}
}


