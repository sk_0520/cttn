/**
DBを適当に扱う。
*/
module cttn.dbmanager;

import std.regex;
import std.datetime;
import std.variant;

import d2sqlite3;

import cttn.type;

/***/
class CommandExpr
{
	bool conditional;

	bool   trueIsCommand;
	string trueCommand;
	CommandExpr trueExpr;
	
	bool   falseIsCommand;
	string falseCommand;
	CommandExpr falseExpr;

	string toCommad() const
	{
		bool isCommand;
		string command;
		if(this.conditional) {
			isCommand = this.trueIsCommand;
			command = this.trueCommand;
		} else {
			isCommand = this.falseIsCommand;
			command = this.falseCommand;
		}
		if(isCommand) {
			return command;
		} else {
			if(conditional) {
				return this.trueExpr.toCommad();
			} else {
				return this.falseExpr.toCommad();
			}
		}
	}
}

/***/
class ExprMap
{
	CommandExpr[string] map;

	alias map this;

	
	CommandExpr createExpr()
	{
		return new CommandExpr();
	}

	void setExpr(string key, string trueCommand)
	{
		setExpr(key, true, trueCommand);
	}
	void setExpr(string key, bool conditional, string trueCommand)
	{
		setExpr(key, conditional, trueCommand, string.init);
	}
	void setExpr(string key, bool conditional, string trueCommand, string falseCommand)
	{
		auto expr = createExpr();
		expr.conditional = conditional;
		expr.trueIsCommand = true;
		expr.trueCommand = trueCommand;
		expr.falseIsCommand = true;
		expr.falseCommand = falseCommand;
		this.map[key] = expr;
	}
}

class DBQuery
{
	protected Query query;
	
	this(Query query)
	{
		this.query = query;
		//this.query.clearBindings();
		this.query.reset();
	}

	void setParam(T)(string key, T value)
	{
		auto useKey = key;
		if(useKey[0] != ':') {
			useKey = ":" ~ key;
		}
		static if(is(T == enum)) {
			this.query.bind(useKey, toOriginalValue(value));
		} else static if(is(T == SysTime)) {
			this.query.bind(useKey, toDBString(value));
		} else {
			this.query.bind(useKey, value);
		}
	}

	void setParams(Variant[string] params, string _file = __FILE__, int _line = __LINE__)
	{
		foreach(key, value; params) {
			if(value.type == typeid(string)) {
				setParam(key, value.get!string);
			} else if(value.type == typeid(size_t)) {
				setParam(key, value.get!size_t);
			} else if(value.type == typeid(sizediff_t)) {
				setParam(key, value.get!sizediff_t);
			} else if(value.type == typeid(int)) {
				setParam(key, value.get!int);
			} else if(value.type == typeid(uint)) {
				setParam(key, value.get!uint);
			} else {
				throw new Exception(key ~ ": " ~ value.toString(), null, _file, _line);
			}
		}
	}



	void execute()
	{
		this.query.execute();
	}

	QueryCache getResultList()
	{
		return QueryCache(this.query.rows());
	}
	
}

class DBTransactionOption
{
}

class DBTransaction
{
	invariant()
	{
		assert(this.db);
	}
	
	protected DBManager db;
	protected DBTransactionOption option;
	
	this(DBManager db, DBTransactionOption option)
	{
		this.db     = db;
		this.option = option;

		begin();
	}

	
	void begin()
	{
		this.db.execute("begin");
	}

	void commit()
	{
		this.db.execute("commit");
	}
	
	void rollback()
	{
		this.db.execute("rollback");
	}
}

class DBManager
{
	protected Database database;

	this() { }
	this(string connection)
	{
		connect(connection, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE);
	}
	
	void connect(string connection, int flags)
	{
		this.database = Database(connection, flags);
	}

	ExprMap createExprMap()
	{
		return new ExprMap();
	}

	DBTransaction beginTransaction(DBTransactionOption option = null)
	{
		return new DBTransaction(this, option);
	}

	/**
	Bugs:
		DBQueryに入れたい。
	*/
	protected string makeQuery(string command, ExprMap exprMap) const
	{
		if(!exprMap || !exprMap.length) {
			//cttn.utility.dprint(command);
			return command;
		}
		
		auto reg = ctRegex!(`\{(\w+)\}`);
		auto result = replaceAll!(cap => "/*!EXP " ~ cap[1] ~ " */" ~ exprMap[cap[1]].toCommad())(command, reg);
		//auto result = replaceAll!(cap => exprMap[cap[1]].toCommad())(command, reg);

		//cttn.utility.dprint(result);
		
		return result;
	}
	

	DBQuery createQuery(string command, ExprMap exprMap = null)
	{
		return new DBQuery(this.database.query(makeQuery(command, exprMap)));
	}
	DBQuery createQuery(string path)(ExprMap exprMap = null)
	{
		return createQuery(import(path), exprMap);
	}
	void execute(string command)
	{
		this.database.execute(command);
	}

	void close()
	{
		cttn.utility.dprint("close");
		this.database.close();
	}
}



