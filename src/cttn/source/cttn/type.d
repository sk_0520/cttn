/**
型。
*/
module cttn.type;

import std.traits;
import std.datetime;
import std.string;

import d2sqlite3;

import cttn.utility;

/**
*/
template toOriginalType(E)
if(is(E == enum))
{
	alias toOriginalType = Unqual!(OriginalType!E);
}
unittest {
	enum E { init = 9 }
	assert(is(toOriginalType!(E) == int));
}


/**
*/
R toOriginalValue(E, R = toOriginalType!E)(E value)
if(is(E == enum))
{
	return cast(R)value;
}
unittest {
	enum E { init = 9 }
	E e;
	assert(toOriginalValue(e) == 9);
}


/**
*/
E fromOriginalValue(E, T)(T dbValue)
if(!is(T == ColumnData))
{
	return cast(E)dbValue;
}


/**
*/
E fromOriginalValue(E)(ColumnData column)
{
	return fromOriginalValue!(E)(column.as!(toOriginalType!(E)));
}

string toDBString(const ref SysTime time)
{
	return time.toSystemUseTime().toISOString();
}
SysTime dbValueToSysTime(string s)
{
	return SysTime.fromISOString(s);
}

string toFileString(const ref SysTime time)
{
	return format("%04s-%02d-%02sT%02s-%02s-%02s", time.year, time.month, time.day, time.hour, time.minute, time.second);
}
string toShowString(const ref SysTime time)
{
	return format("%04s/%02d/%02s %02s:%02s:%02s", time.year, time.month, time.day, time.hour, time.minute, time.second);
}
string toShowDateString(const ref SysTime time)
{
	return format("%04s/%02d/%02s", time.year, time.month, time.day);
}

