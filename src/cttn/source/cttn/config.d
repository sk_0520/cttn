/**
設定。
*/
module cttn.config;

import std.algorithm;
import std.file;
import std.stream;
import std.string;
import std.regex;

import cttn.literal;
import cttn.utility;

/**
基本設定。
*/
class CttnConfig
{
	uint thumbnailWidth    = Literal.thumbnailWidth;
	uint thumbnailHeight   = Literal.thumbnailHeight;

	string pathDataDir     = Literal.pathDataDir;
	string pathCacheDir    = Literal.pathCacheDir;
	
	string adminName   = Literal.adminName;
	string siteName    = Literal.siteName;
	string hostName    = Literal.hostName;
	string mailAddress = Literal.mailAddress;

	string[] bindAddresses = Literal.serverBind;
	ushort   port = Literal.serverPort;
	ulong    maxRequestSize = Literal.maxRequestSize;

	string[] programRootList = Literal.programRootList;
	string moduleController = Literal.moduleController;
	string moduleModel      = Literal.moduleModel;

	string loginName = Literal.loginName;
	string loginPass = Literal.loginPass;

	size_t articleShowLength = Literal.articleShowLength;


	///
	this() { }


	/**
	*/
	this(string path)
	{
		if(exists(Literal.pathCttnConfig)) {
			auto stream = new EndianStream(new BufferedFile(path));
			scope(exit) {
				stream.close();
			}
			stream.readBOM();
			mergeConfig(stream);
		}
	}


	protected void mergeConfig_Impl(string[string] map)
	{
		void loader(T)(string key, ref T result)
		{
			if(auto p = key in map) {
				convertFromString(*p, result);
			}
		}
		loader("adminName", this.adminName);
		loader("siteName", this.siteName);
		loader("hostName", this.hostName);
		loader("mailAddress", this.mailAddress);
		loader("port", this.port);
		loader("maxRequestSize", this.maxRequestSize);
		loader("loginName", this.loginName);
		loader("loginPass", this.loginPass);
		loader("pathDataDir", this.pathDataDir);
		loader("pathCacheDir", this.pathCacheDir);
		
	}
	
	void mergeConfig(scope InputStream stream)
	{
		string[string] map;
		auto reg = regex(`(\w+)\s*=\s*(.*)`);
		foreach(ulong n, char[] rawLine; stream) {
			auto line = rawLine.idup.strip();
			if(!line.length || line.length < "K=V".length) {
				continue;
			}
			if(line[0] == '#') {
				continue;
			}
			
			auto m = match(line, reg);
			auto c = m.captures;
			
			auto key   = c[1];
			auto value = c[2];
			map[key] = value;
		}
		mergeConfig_Impl(map);
	}

	bool isTopPage(string reqPath) const
	{
		return any!(s => s == reqPath)(programRootList);
	}
}


