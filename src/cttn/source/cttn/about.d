/**
cttn情報。
*/
module cttn.about;

struct CttnAbout
{
	static immutable:
	auto appName      = "cttn.cgi";
	auto appVersion   = "0.30";
	auto webSite      = "http://content-type-text.net";
	auto repository   = "https://bitbucket.org/sk_0520/cttn";
}

