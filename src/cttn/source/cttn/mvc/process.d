/**
なんちゃってMVC。
*/
module cttn.mvc.process;

import std.algorithm;
import std.exception;
import std.conv;
import std.file;
import std.datetime;

import vibe.d;

import cttn.config;
import cttn.utility;
import cttn.mvc.model;
import cttn.mvc.login;
import cttn.mvc.app_common;
import cttn.mvc.params;



/**
共通処理部分。
*/
abstract class CttnProcess
{
	/// 要求データ。
	protected CttnAppCommonData rawCommon;

	protected void enforceProcess() { }

	/**
	*/
	this(CttnAppCommonData rawCommonData)
	{
		this.rawCommon = rawCommonData;
		if(cast(CttnTargetBaseProcess)this is null) {
			enforceProcess();
		}
	}

	/**
	実行部分。
	
	CttnTargetProcessを使用しないのであればユーザーコードで使用する。

	Params:
		拡張パラメータ。
	*/
	ExecuteResult execute(Parameter exRawParam);

	/**
	キャッシュさせない
	*/
	protected void setHeaderNoCache()
	{
		this.rawCommon.res.headers["Pragma"] = "no-cache";
		this.rawCommon.res.headers["Cache-Control"] = "no-cache";
	}

}

private abstract class CttnTargetBaseProcess: CttnProcess
{
	this(CttnAppCommonData rawCommonData)
	{
		super(rawCommonData);
	}
}

/**
*/
abstract class CttnTargetProcess(TModel: CttnModel, TParam = Parameter): CttnTargetBaseProcess
{
	///
	TCttnAppCommonData!(TModel) common;

	/***/
	this(TCttnAppCommonData!(TModel) commonData)
	{
		super(commonData);
		this.common = commonData;
		
		enforceProcess();
	}
	
	/**
	実行部分。
	
	ユーザーコードでは使用しない。

	Params:
		拡張パラメータ。
	*/
	override ExecuteResult execute(Parameter exRawParam)
	{
		auto exParam = cast(TParam)exRawParam;
		return executeTarget(exParam);
	}
	
	/**
	実行部分。
	
	ユーザーコードで使用する。

	Params:
		拡張パラメータ。
	*/
	ExecuteResult executeTarget(TParam exParam);
}

/**
CttnProcessコンストラクタ実装。
*/
mixin template CttnProcessBuilder()
{
	import cttn.mvc.app_common;
	
	this(CttnAppCommonData rawCommonData)
	{
		super(rawCommonData);
	}
}

/**
CttnTargetProcessコンストラクタ実装。
*/
mixin template CttnTargetProcessBuilder(TModel)
{
	import cttn.mvc.app_common;
	
	this(TCttnAppCommonData!(TModel) commonData)
	{
		super(commonData);
	}
}


interface IInitializeProcess
{
	@property
	{
		bool initialized() const;
		void initialized(bool value);
	}
}

mixin template InitializeProcessBuilder()
{
	private bool _initialized;
	
	override @property
	{
		bool initialized() const
		{
			return this._initialized;
		}
		void initialized(bool value)
		{
			this._initialized = value;
		}
	}
}


