/**
使い回しデータ。
*/
module cttn.mvc.params;


/**
*/
class ExecuteResult
{
	@property
	static ExecuteResult success()
	{
		return new class ExecuteResult {
			this()
			{
				this.eval = true;
			}
		};
	}
	@property
	static ExecuteResult failure()
	{
		return new class ExecuteResult {
			this()
			{
				this.eval = false;
			}
		};
	}
	
	bool eval;

	this() { }
	this(bool eval)
	{
		this.eval = eval;
	}

	@property
	void preservationTrue(bool value)
	{
		if(!value) {
			this.eval = value;
		}
	}
}


/**
Builder
*/
abstract class Parameter
{
}


