/**
なにかしらの記憶媒体からデータ取得
*/
module cttn.mvc.storage;

import cttn.dbmanager;

/**
*/
abstract class CttnStorage
{
	/// 媒体へのアクセス手段
	protected void* rawAccess;

	/**
	*/
	this(void* rawAccess)
	{
		this.rawAccess = rawAccess;
	}
}


/**
*/
abstract class DBStorage: CttnStorage
{
	protected DBManager db;
	protected DBTransaction tran;
	
	this(DBManager dbManager)
	in
	{
		assert(dbManager);
	}
	body
	{
		super(cast(void*)dbManager);
		this.db = dbManager;
	}

	DBTransaction beginTransaction(DBTransactionOption option = null)
	{
		return this.tran = this.db.beginTransaction(option);
	}
	void commit()
	{
		this.tran.commit();
	}
	void rollback()
	{
		this.tran.rollback();
	}
}


/**
*/
mixin template DBStorageBuilder()
{
	import cttn.dbmanager;
	
	this(DBManager dbManager)
	{
		super(dbManager);
	}
}

