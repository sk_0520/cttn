/**
なんちゃってMVC。
*/
module cttn.mvc.app_common;

import std.algorithm;
import std.exception;
import std.conv;
import std.file;
import std.datetime;

import vibe.d;

import cttn.config;
import cttn.utility;
import cttn.mvc.login;
import cttn.mvc.params;
import cttn.mvc.model;
import cttn.mvc.db;



/**
共通受け渡しデータ。
*/
class CttnAppCommonData
{
	/// 要求データ。
	HTTPServerRequest  req;
	/// 応答データ。
	HTTPServerResponse res;
	/// 設定データ。
	const CttnConfig  conf;
	/// 基底モデル。ユーザーコードでは使用しない。
	CttnModel          rawModel;
	/// DB
	DB db;
	/// セッションデータ
	Login login;

	this(HTTPServerRequest req, HTTPServerResponse res, DB db, const CttnConfig conf, ref Login login, CttnModel rawModel)
	in
	{
		assert(req, __FUNCTION__ ~ ": req!");
		assert(res, __FUNCTION__ ~ ": res!");
		assert(db, __FUNCTION__ ~ ": db!");
		assert(conf, __FUNCTION__ ~ ": conf!");
		assert(rawModel, __FUNCTION__ ~ ": rawModel!");
	}
	body
	{
		this.req      = req;
		this.res      = res;
		this.db  = db;
		this.conf  = conf;
		this.login  = login;
		this.rawModel = rawModel;
	}
}

class TCttnAppCommonData(TModel: CttnModel): CttnAppCommonData
{
	/// モデル。
	TModel model;
	this(HTTPServerRequest req, HTTPServerResponse res, DB db, const CttnConfig conf, ref Login login, TModel model)
	in
	{
		assert(req, __FUNCTION__ ~ ": req?");
		assert(res, __FUNCTION__ ~ ": res?");
		assert(conf, __FUNCTION__ ~ ": conf_");
		assert(model, __FUNCTION__ ~ ": model?");
	}
	out
	{
		assert(this.req, __FUNCTION__ ~ ": req");
		assert(this.res, __FUNCTION__ ~ ": res");
		assert(this.conf, __FUNCTION__ ~ ": conf");
		assert(this.model, __FUNCTION__ ~ ": model");
	}
	body
	{
		super(req, res, db, conf, login, model);
		this.model = model;
	}
}




