/**
なんちゃってMVC。
*/
module cttn.mvc.controller;

import std.algorithm;
import std.exception;
import std.conv;
import std.file;
import std.datetime;

import vibe.d;

import cttn.config;
import cttn.literal;
import cttn.utility;
import cttn.dbmanager;
import cttn.mvc.params;
import cttn.mvc.model;
import cttn.mvc.login;
import cttn.mvc.app_common;
import cttn.mvc.db;
import cttn.app.storage.cache_storage;
import cttn.app.process.log_process;



/**
共通コントローラ。
*/
abstract class CttnController
{
	protected CttnAppCommonData rawCommon;

	private static string makePageName(string page, ViewMode mode)
	{
		/+
		string viewName;
		final switch(mode) {
			case ViewMode.pc:      viewName = "pc";      break;
			case ViewMode.touch:   viewName = "touch";   break;
			case ViewMode.compact: viewName = "compact"; break;
		}

		return page ~ "." ~ viewName ~ ".dt";
		+/
		return page ~ ".dt";
	}

	protected void buildCommonData(HTTPServerRequest req, HTTPServerResponse res, DB db, const CttnConfig conf, ref Login login, CttnModel rawModel)
	in
	{
		assert(req, "in-req");
		assert(res, "in-res");
		assert(conf, "in-conf");
		assert(rawModel, "in-rawModel");
	}
	out
	{
		assert(this.rawCommon);
		assert(this.rawCommon.req, "rawModel.req");
		assert(this.rawCommon.res, "rawModel.rea");
		assert(this.rawCommon.conf, "rawCommon.conf");
		assert(this.rawCommon.rawModel, "rawCommon.rawModel");
	}
	body
	{
		this.rawCommon = new CttnAppCommonData(req, res, db, conf, login, rawModel);
	}

	/**
	サービス受付部分。

	ユーザーコードでは使用しない。
	*/
	final void listenService(HTTPServerRequest req, HTTPServerResponse res, DB db, const CttnConfig conf, CttnModel rawModel)
	in
	{
		assert(req, __FUNCTION__~": req");
		assert(res, __FUNCTION__~": res");
		assert(conf, __FUNCTION__~": conf");
		assert(rawModel, __FUNCTION__~": rawModel");
	}
	body
	{
		Login login;
		if(req.session) {
			auto sessionStorage = new SessionStorage(req.session);
			auto sessionKey = sessionStorage.getKey(Literal.sessionKey);
			if(sessionKey !is null) {
				auto storage = new LoginSessionStorage(db.cache);
				login = storage.findLoginSessionKey(sessionKey);
				if(login.isLogin) {
					login.isLogin = getNowTime() - login.lastAccess <= Literal.sessionTime;
				}
			}
		}
		
		buildCommonData(req, res, db, conf, login, rawModel);
		this.rawCommon.rawModel.importRequest(req);

		/* //ちょっと実験
		// そもそもログみねーし一時的に除外
		version(none)
		*/
		// ログイン済みはいらん
		/+
		if(!this.rawCommon.login.isLogin) {
			// アクセスログ記録。
			synchronized(this.rawCommon.db.log) {
				auto logProcess = new LogAccessProcess(this.rawCommon);
				logProcess.execute(Parameter.init);
			}
		}
		+/
		doProcess();
	}
	/**
	サービス処理部分。
	
	CttnTargetControllerを使用しないのであればユーザーコードで使用する。
	*/
	protected void doProcess();

	protected static string render_Impl(string commonName, string modelName)
	{
		return `
		
		// template引数用に設定
		auto res   = this.rawCommon.res;
		auto req   = this.rawCommon.req;
		auto conf  = this.rawCommon.conf;
		auto db    = this.rawCommon.db;
		auto login = this.rawCommon.login;
		auto model = this.` ~ commonName ~ `.` ~ modelName ~ `;

		assert(model);

		auto defaultRender = true;
		foreach(viewMode; viewModeList) {
			if(model.view == viewMode) {
				res.render!(makePageName(name, viewMode), res, req, conf, db, login, model);
				defaultRender = false;
				break;
			}
		}
		if(defaultRender) {
			this.rawCommon.res.render!(makePageName(name, viewModeList[0]), res, req, conf, db, login, model);
		}
		
		`;
	}
	protected void render(string name, viewModeList...)()
	{
		mixin(render_Impl("rawCommon", "rawModel"));
	}


}


/**
型指定共通コントローラ。

基本的にこちらを使用する。
*/
abstract class CttnTargetController(TModel: CttnModel): CttnController
{
	TCttnAppCommonData!(TModel) common;
	
	override protected void buildCommonData(HTTPServerRequest req, HTTPServerResponse res, DB db, const CttnConfig conf, ref Login login, CttnModel rawModel)
	in
	{
		assert(cast(TModel)rawModel, __FUNCTION__ ~ ": cast(" ~ TModel.stringof ~ ")rawModel");
	}
	out
	{
		assert(this.common);
		assert(this.common.model, "this.common.model is null!");
	}
	body
	{
		this.common = new TCttnAppCommonData!(TModel)(req, res, db, conf, login, cast(TModel)rawModel);
		
		this.rawCommon = this.common;
	}
	
	/**
	サービス処理部分。
	
	ユーザーコードでは使用しない。
	*/
	override protected void doProcess()
	{
		doTargetProcess();
	}
	
	/**
	サービス処理部分。

	ユーザーコードで使用する。
	*/
	protected void doTargetProcess();

	protected void render(string name, viewModeList...)()
	{
		mixin(render_Impl("common", "model"));
	}
}

