/**
なんちゃってMVC。
*/
module cttn.mvc.model;

import std.algorithm;
import std.array;
import std.exception;
import std.conv;
import std.file;
import std.datetime;
import std.traits;
import std.regex;

import vibe.d;

import cttn.config;
import cttn.type;
import cttn.utility;


/**
画面表示種別。
*/
enum ViewMode
{
	/// パソコン。
	pc,
	/// スマホとか。
	touch,
	/// ガラケー的な。
	compact,
}


/**
イベント共通部。
*/
enum Event
{
	init = "init"
}



/**
共通モデル。
*/
class CttnModel
{
	/// 表示種別。
	ViewMode view  = ViewMode.pc;
	/// イベント。
	string   event = Event.init;
	/// 処理開始時間。
	private SysTime _startTime;
	@property
	SysTime startTime() const
	{
		return this._startTime;
	}

	/**
	現在までの経過時間。
	*/
	@property
	final Duration dur() const
	{
		return Clock.currTime - this.startTime;
	}

	/**
	Note:
		もうちっとスマートにこう。。。
	*/
	private static string convertValue_Impl(bool dg)
	{
		auto singleArg = ["*p", "result"];
		auto multiArg  = ["v",  "r"];
		if(dg) {
			singleArg ~= "dg";
			multiArg  ~= "dg";
		}
		string singleValue = singleArg.join(",");
		string multiValue = multiArg.join(",");

		string process = `
			static if(!isSomeString!T && isArray!T) {
				alias T0 = Unqual!(typeof(T.init[0]));
				static if(is(T0 == enum)) {
					alias T1 = toOriginalType!T0;
				} else {
					alias T1 = T0;
				}
				T1 r;
				auto app = appender!(T0[])();
				Exception resultEx;
				foreach(v; req.form.getAll(key)) {
					auto ex = convertFromString!(T1)(` ~ multiValue ~ `);
					if(!resultEx) {
						resultEx = ex;
					}
					app.put(cast(T0)r);
				}
				result = app.data();
				return resultEx;
			} else {
				//dprint(*p);
				return convertFromString!(T)(` ~ singleValue ~ `);
			}
		`;
		
		return `
		if(auto p = key in req.query) {
			` ~ process ~ `
		} else if(auto p = key in req.form) {
			` ~ process ~ `
		}

		return new Exception("not found: " ~ key);
		`;
	}

	/**
	要求を指定データに落とし込む。
	*/
	protected static Exception convertValue(T)(in HTTPServerRequest req, string key, ref T result)
	{
		mixin(convertValue_Impl(false));
	}
	protected static Exception convertValue(T)(in HTTPServerRequest req, string key, ref T result, T delegate(string s) dg)
	{
		mixin(convertValue_Impl(true));
	}

	/**
	あれやこれやとる場合は独自拡張で。
	
	リフレクション的な物作れなかった。
	*/
	void importRequest(in HTTPServerRequest req)
	{
		this._startTime = req.timeCreated;
		convertValue(req, "event", this.event);
		auto viewException = convertValue(req, "view", this.view);

		// UA から振り分け
		if(viewException) {
			if(auto p = "User-Agent" in req.headers) {
				auto ua = *p;
				auto reg = ctRegex!(`iPhone|Android`, "i");
				if(matchFirst(ua, reg).length) {
					this.view = ViewMode.touch;
				}
			}
		}
	}
}



