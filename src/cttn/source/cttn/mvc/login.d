/**
サービス。
*/
module cttn.mvc.login;

import std.algorithm;
import std.array;
import std.file;

import vibe.d;

import cttn.literal;
import cttn.config;
import cttn.mvc;
import cttn.utility;
import cttn.dbmanager;


/**/
struct Login
{
	string  key;
	string  name;
	SysTime lastAccess;
	bool    isLogin;
}


