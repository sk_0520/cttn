/**
データベース。
*/
module cttn.mvc.db;

import d2sqlite3;

import cttn.utility;
import cttn.dbmanager;

class DB
{
	DBManager data;
	DBManager log;
	DBManager cache;

	this(string dataDbPath, string logDbPath)
	{
		createParentDir(dataDbPath);
		createParentDir(logDbPath);
		
		//this.data  = new DBManager(dataDbPath, SharedCache.enabled);
		//this.log   = new DBManager(logDbPath, SharedCache.enabled);
		//this.cache = new DBManager(":memory:", SharedCache.enabled);
		this.data  = new DBManager(dataDbPath);
		this.log   = new DBManager(logDbPath);
		this.cache = new DBManager(":memory:");
	}

	void close()
	{
		this.data.close();
		this.log.close();
	}
}

