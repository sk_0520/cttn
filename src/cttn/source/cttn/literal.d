/**
定数。
*/
module cttn.literal;

import std.datetime;

/**
*/
struct Literal
{
	static immutable:

	uint thumbnailWidth  = 120;
	uint thumbnailHeight = 160;
	string thumbnailSuffix = "thumbnail";
	
	string pathCttnConfig  = "/etc/cttn/cttn.conf";
	//string pathSessionFile = "/tmp/cttn/cttn-session.dat";
	string pathCacheDir    = "/dev/shm/cttn";
	version(linux) {
		string pathDataDir     = "/home/slave/cttn-dat";
	} else {
		string pathDataDir     = "../~data";
	}
	string nameDataDB = "data.sqlite3";
	string nameLogDB  = "log.sqlite3";
	string nameFiles  = "files";

	string sessionKey = "key";
	auto   sessionTime = dur!("minutes")(60);
	
	size_t blogLimitPage = 10;
	
	size_t articleShowLength = 5;

	string adminName   = "author";
	string siteName    = "site-name";
	string hostName    = "example.net";
	string mailAddress = "mail@address";
	
	string[] serverBind = null;
	ushort   serverPort = 8080;
	ulong    maxRequestSize = 50 * 1024 * 1024;
	
	string[] programRootList = ["/", "/app", "/app/*"];
	string moduleController = "cttn.app.controller.${module}_controller.${class}Controller";
	string moduleModel      = "cttn.app.model.${module}_model.${class}Model";

	string loginName = "admin";
	/// std.digest.sha.SHA1Digest.digest: pass
	string loginPass = "9D4E1E23BD5B727046A9E3B4B7DB57BD8D6EE684";

}
