/**
サービス。
*/
module cttn.service;

import std.algorithm;
import std.array;
import std.file;
import std.path;
import std.string;

import vibe.d;

import cttn.literal;
import cttn.config;
import cttn.mvc;
import cttn.utility;
import cttn.dbmanager;

import cttn.app.controller.error_controller;
import cttn.app.controller.top_controller;
import cttn.app.model.error_model;
import cttn.app.model.top_model;
import cttn.app.process.log_process;


private __gshared DB serviceDB;

/**
*/
void startService()
{
	version(linux) {
		auto conf = new CttnConfig(Literal.pathCttnConfig);
	} else {
		auto conf = new CttnConfig();
	}

	auto dataDbPath = buildPath(conf.pathDataDir, Literal.nameDataDB);
	auto logDbPath = buildPath(conf.pathDataDir, Literal.nameLogDB);
	auto db = new DB(dataDbPath, logDbPath);
	// shared の使い方が未だにわからん、てかこれスレッドでサービス起動してるなら全部sharedじゃないとダメなんじゃ。。。
	serviceDB = db;
	
	auto query = db.cache.createQuery!("cache-table.sql");
	query.execute();
	
	auto httpServerSettings = new HTTPServerSettings;
	if(conf.bindAddresses.length) {
		httpServerSettings.bindAddresses = conf.bindAddresses;
	}
	httpServerSettings.port = conf.port;
	httpServerSettings.maxRequestSize = conf.maxRequestSize;
	httpServerSettings.sessionStore = new MemorySessionStore;

	auto fileServerSettings = new HTTPFileServerSettings;
	fileServerSettings.encodingFileExtension = ["gzip" : ".gz"];

	auto router = new URLRouter;
	router.get("/gzip/*", serveStaticFiles("./public/", fileServerSettings));
	router.get("/*",      serveStaticFiles("./public/"));
	auto normalDg = (HTTPServerRequest req, HTTPServerResponse res) {
		listenService(req, res, conf, db);
	};
	foreach(rootPath; conf.programRootList) {
		router.any(rootPath, normalDg);
	}
	auto errorDg = (HTTPServerRequest req, HTTPServerResponse res, HTTPServerErrorInfo error) {
		listenError(req, res, error, conf, db);
	};
	httpServerSettings.errorPageHandler = errorDg;

	listenHTTP(httpServerSettings, router);
}

void stopService()
{
	// 終了処理したいけどメモリは何処...
	serviceDB.close();
}

void listenError(HTTPServerRequest req, HTTPServerResponse res, HTTPServerErrorInfo error, const CttnConfig conf, DB db)
{
	auto controller = new ErrorController();
	auto model      = new ErrorModel();
	model.error = error;
	controller.listenService(req, res, db, conf, model);
}

/***/
void listenService(HTTPServerRequest req, HTTPServerResponse res, const CttnConfig conf, DB db)
in
{
	assert(req, __FUNCTION__~": req");
	assert(res, __FUNCTION__~": res");
	assert(conf, __FUNCTION__~": conf");
}
body
{
	auto reqPath = req.path;

	CttnController controller;
	CttnModel      model;
	
	if(conf.isTopPage(reqPath)) {
		controller = new TopController();
		model      = new TopModel();
	} else {
		// /app/* の*部分の先頭をモジュールとする
		auto pathList = reqPath.split("/");
		auto moduleName = replace(pathList[2], "-", "_");
		auto classNameHead = capitalize(moduleName);
		if(moduleName.any!(c => '_')) {
			classNameHead = moduleName.split('_').map!(s => capitalize(s)).join("");
		}
		string replaceName(string src)
		{
			return src
				.replace("${module}", moduleName)
				.replace("${class}",  classNameHead)
			;
		}
		auto classNameContrller = replaceName(conf.moduleController);
		auto classNameModel     = replaceName(conf.moduleModel);

		controller = cast(CttnController)Object.factory(classNameContrller);
		if(!controller) {
			return;
		}
		model = cast(CttnModel)Object.factory(classNameModel);
		// モデルは無いこともあるため基本モデルの生成が必要。
		if(!model) {
			model = new CttnModel();
		}
	}
	assert(controller);
	assert(model);

	controller.listenService(req, res, db, conf, model);
}

