/**
いろいろ。
*/
module cttn.utility;

import std.array;
import std.algorithm;
import std.string;
import std.file;
import std.datetime;
import std.path;
import std.stdio;
import std.traits;
import std.conv;
import std.exception;

import cttn.type;

/**
*/
void print(T)(in T msg, string _file = __FILE__, int _line = __LINE__, string _func = __PRETTY_FUNCTION__)
{
	writefln("%s(%s): %s [%s]", _file, _line, msg, _func);
}

/**
*/
void dprint(T)(lazy T msg, string _file = __FILE__, int _line = __LINE__, string _func = __PRETTY_FUNCTION__)
{
	debug print(msg, _file, _line, _func);
}
version(dev)
unittest
{
	dprint("#");
}

/**
変数出力。
*/
void trace(alias VAR)(string _file=__FILE__, int _line=__LINE__, string _func=__PRETTY_FUNCTION__)
{
	auto msg = format("%s = %s", VAR.stringof, VAR);
	print(msg, _file, _line, _func);
}
void dtrace(alias VAR)(string _file=__FILE__, int _line=__LINE__, string _func=__PRETTY_FUNCTION__)
{
	debug trace!(VAR)(_file, _line, _func);
}

/**
要求を指定データに落とし込む。
*/
Exception convertFromString(T)(string value, ref T result)
{
	static if(is(T == enum) && !is(toOriginalType!T == string)) {
		alias T1 = toOriginalType!T;
		T1 r = void;
		auto ex = collectException!(ConvException)(to!(T1)(value), r);
		result = cast(T)r;
		return ex;
	} else {
		return collectException!(ConvException)(to!(T)(value), result);
	}
}


/**
要求を指定データに落とし込む。
*/
Exception convertFromString(T)(string value, ref T result, T delegate(string s) dg)
in
{
	assert(dg);
}
body
{
	/+
	static if(is(T == enum)) {
		alias T1 = toOriginalType!T;
		T1 r = void;
		auto ex = collectException!(Exception)(dg(value), r);
		result = cast(T)r;
		return ex;
	} else {
		return collectException!(Exception)(dg(value), result);
	}
	+/
		return collectException!(Exception)(dg(value), result);
}


void createParentDir(string filePath)
{
	auto parentDir = dirName(filePath);
	if(!exists(parentDir)) {
		mkdirRecurse(parentDir);
	}
}


SysTime toSystemUseTime(SysTime time)
{
	time.fracSec = FracSec.from!"hnsecs"(0);
	return time;
}
SysTime getNowTime()
{
	return toSystemUseTime(Clock.currTime());
}


/**
作業フォルダの構築
*/
void makeDir(string dirPath)
{
	auto app = appender!(char[])();
	foreach(singlePath; pathSplitter(dirPath)) {
		app.put(singlePath);
		if(!exists(app.data)) {
			mkdir(app.data);
		}
		app.put(dirSeparator);
	}
}
/***/
string makeParentDir(string filePath)
{
	auto dirPath = dirName(filePath);
	makeDir(dirPath);
	return dirPath;
}
